---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_2be1f0e022faf424f18f30275e61416e -->
## Authentication
Авторизация в системе

> Example request:

```bash
curl -X POST "http://mergen/api/v1/auth/login" \
-H "Accept: application/json" \
    -d "phone"="magni" \
    -d "password"="magni" \
    -d "platform"="ios" \
    -d "version"="magni" \
    -d "model"="magni" \
    -d "uuid"="magni" \
    -d "push_token"="magni" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/auth/login",
    "method": "POST",
    "data": {
        "phone": "magni",
        "password": "magni",
        "platform": "ios",
        "version": "magni",
        "model": "magni",
        "uuid": "magni",
        "push_token": "magni"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/auth/login`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    phone | string |  required  | 
    password | string |  required  | 
    platform | string |  optional  | `ios` or `android`
    version | string |  optional  | 
    model | string |  optional  | 
    uuid | string |  optional  | Allowed: alpha-numeric characters, as well as dashes and underscores.
    push_token | string |  optional  | 

<!-- END_2be1f0e022faf424f18f30275e61416e -->

<!-- START_3157fb6d77831463001829403e201c3e -->
## Register an User
Регистрация пользователя

> Example request:

```bash
curl -X POST "http://mergen/api/v1/auth/register" \
-H "Accept: application/json" \
    -d "phone"="ut" \
    -d "password"="ut" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/auth/register",
    "method": "POST",
    "data": {
        "phone": "ut",
        "password": "ut"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/auth/register`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    phone | string |  required  | 
    password | string |  required  | 

<!-- END_3157fb6d77831463001829403e201c3e -->

<!-- START_80420c095ed96da032c9eb419d7d6e2d -->
## List Categories
Отобразить все категории

> Example request:

```bash
curl -X GET "http://mergen/api/v1/categories" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/categories",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/categories`


<!-- END_80420c095ed96da032c9eb419d7d6e2d -->

<!-- START_2378770b4f57b93f810abda7c44614b8 -->
## Show a category
Показать категорию

> Example request:

```bash
curl -X GET "http://mergen/api/v1/categories/{category}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/categories/{category}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/categories/{category}`


<!-- END_2378770b4f57b93f810abda7c44614b8 -->

<!-- START_eff3de500a74b38af2f02b1ee79df2db -->
## List News
Отобразить все новости

> Example request:

```bash
curl -X GET "http://mergen/api/v1/news" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/news",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/news`


<!-- END_eff3de500a74b38af2f02b1ee79df2db -->

<!-- START_aa699136ba2aa9c6ed03dfb8f04c63a0 -->
## Show a News item
Показать новость

> Example request:

```bash
curl -X GET "http://mergen/api/v1/news/{news}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/news/{news}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/news/{news}`


<!-- END_aa699136ba2aa9c6ed03dfb8f04c63a0 -->

<!-- START_55b3e4a32ca6ec34e112e57ee11940fb -->
## List Products
Показать список товаров

> Example request:

```bash
curl -X GET "http://mergen/api/v1/categories/{category}/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/categories/{category}/products",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/categories/{category}/products`


<!-- END_55b3e4a32ca6ec34e112e57ee11940fb -->

<!-- START_4f3fc61b623e1e14450da21c184e47b8 -->
## Show a Product item by id and category
Показать деталь продукта

> Example request:

```bash
curl -X GET "http://mergen/api/v1/categories/{category}/products/{product}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/categories/{category}/products/{product}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/categories/{category}/products/{product}`


<!-- END_4f3fc61b623e1e14450da21c184e47b8 -->

<!-- START_70f33cfcbaeef1991de5040b48f89272 -->
## List Delivery areas
Отобразить места доставки

> Example request:

```bash
curl -X GET "http://mergen/api/v1/delivery-areas" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/delivery-areas",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/delivery-areas`


<!-- END_70f33cfcbaeef1991de5040b48f89272 -->

<!-- START_40023b4da249d52ab56a2352b31ad465 -->
## Check to Promo code
Проверить промо код

> Example request:

```bash
curl -X GET "http://mergen/api/v1/promo-code" \
-H "Accept: application/json" \
    -d "code"="officia" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/promo-code",
    "method": "GET",
    "data": {
        "code": "officia"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": {
        "code": [
            "The code field is required."
        ]
    },
    "messages": []
}
```

### HTTP Request
`GET api/v1/promo-code`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    code | string |  required  | 

<!-- END_40023b4da249d52ab56a2352b31ad465 -->

<!-- START_cedc85e856362e0e3b46f5dcd9f8f5d0 -->
## Show an user resource
Показать объект пользователя

> Example request:

```bash
curl -X GET "http://mergen/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/users/{user}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/users/{user}`


<!-- END_cedc85e856362e0e3b46f5dcd9f8f5d0 -->

<!-- START_296fac4bf818c99f6dd42a4a0eb56b58 -->
## Update an user resource
Обновить данные пользователя

> Example request:

```bash
curl -X PUT "http://mergen/api/v1/users/{user}" \
-H "Accept: application/json" \
    -d "name"="ab" \
    -d "first_name"="ab" \
    -d "last_name"="ab" \
    -d "middle_name"="ab" \
    -d "company"="ab" \
    -d "address"="ab" \
    -d "password"="ab" \
    -d "city_id"="9" \
    -d "email"="tromp.madalyn@example.org" \
    -d "image_id"="9" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/users/{user}",
    "method": "PUT",
    "data": {
        "name": "ab",
        "first_name": "ab",
        "last_name": "ab",
        "middle_name": "ab",
        "company": "ab",
        "address": "ab",
        "password": "ab",
        "city_id": 9,
        "email": "tromp.madalyn@example.org",
        "image_id": 9
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/users/{user}`

`PATCH api/v1/users/{user}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | 
    first_name | string |  optional  | 
    last_name | string |  optional  | 
    middle_name | string |  optional  | 
    company | string |  optional  | 
    address | string |  optional  | 
    password | string |  optional  | 
    city_id | integer |  optional  | 
    email | email |  optional  | 
    image_id | integer |  optional  | 

<!-- END_296fac4bf818c99f6dd42a4a0eb56b58 -->

<!-- START_22354fc95c42d81a744eece68f5b9b9a -->
## Destroy an user resource
Удалить пользователя

> Example request:

```bash
curl -X DELETE "http://mergen/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/users/{user}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/users/{user}`


<!-- END_22354fc95c42d81a744eece68f5b9b9a -->

<!-- START_25bf4092f5e200124a149897733aac34 -->
## List my own orders
Показать список заказов

> Example request:

```bash
curl -X GET "http://mergen/api/v1/orders" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/orders",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/orders`


<!-- END_25bf4092f5e200124a149897733aac34 -->

<!-- START_c79cb2035f69ac8078c2cec9fc2fab4a -->
## Order create
Создать заказ с продуктами
 products[1][id], products[1][cost]

> Example request:

```bash
curl -X POST "http://mergen/api/v1/orders" \
-H "Accept: application/json" \
    -d "products.1.id"="et" \
    -d "products.1.cost"="et" \
    -d "products.1.quantity"="339884" \
    -d "type_payment"="2" \
    -d "comment"="et" \
    -d "delivery_type"="1" \
    -d "available_sum"="339884" \
    -d "delivery_address"="et" \
    -d "delivery_date"="2001-05-20" \
    -d "promo_code"="et" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/orders",
    "method": "POST",
    "data": {
        "products.1.id": "et",
        "products.1.cost": "et",
        "products.1.quantity": 339884,
        "type_payment": "2",
        "comment": "et",
        "delivery_type": "1",
        "available_sum": 339884,
        "delivery_address": "et",
        "delivery_date": "2001-05-20",
        "promo_code": "et"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/orders`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    products.1.id | string |  required  | 
    products.1.cost | string |  required  | 
    products.1.quantity | numeric |  required  | 
    type_payment | integer |  optional  | `1` or `2`
    comment | string |  optional  | 
    delivery_type | integer |  optional  | `1`
    available_sum | numeric |  optional  | 
    delivery_address | string |  optional  | 
    delivery_date | date |  optional  | 
    promo_code | string |  optional  | 

<!-- END_c79cb2035f69ac8078c2cec9fc2fab4a -->

<!-- START_b4bbc4b1b4c4ddc2effe9e5e2475dd8d -->
## Show a Order detail
Показать деталь заказа

> Example request:

```bash
curl -X GET "http://mergen/api/v1/orders/{order}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/orders/{order}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/orders/{order}`


<!-- END_b4bbc4b1b4c4ddc2effe9e5e2475dd8d -->

<!-- START_f34ad9d71f18dd67576cc6db60268192 -->
## Remove the order
Удалить заказ

> Example request:

```bash
curl -X DELETE "http://mergen/api/v1/orders/{order}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/orders/{order}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/orders/{order}`


<!-- END_f34ad9d71f18dd67576cc6db60268192 -->

<!-- START_ea1ce6c810b8ce1d655cde0dc2bd7b11 -->
## Show list orders from admin
Показать список заказов для авмина

> Example request:

```bash
curl -X GET "http://mergen/api/v1/admin/orders" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/admin/orders",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": false,
    "content": null,
    "errors": [],
    "messages": []
}
```

### HTTP Request
`GET api/v1/admin/orders`


<!-- END_ea1ce6c810b8ce1d655cde0dc2bd7b11 -->

<!-- START_18631bafed775928278e5dd18f835d97 -->
## Moderate order from admin
Модерация заказов

> Example request:

```bash
curl -X POST "http://mergen/api/v1/admin/orders/{id}/moderate" \
-H "Accept: application/json" \
    -d "status"="reject" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://mergen/api/v1/admin/orders/{id}/moderate",
    "method": "POST",
    "data": {
        "status": "reject"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/admin/orders/{id}/moderate`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    status | string |  required  | `confirm`, `reject` or `complete`

<!-- END_18631bafed775928278e5dd18f835d97 -->

