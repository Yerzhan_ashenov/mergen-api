$( document ).ready(function() {

    $("select#places").change(function()
    {
       document.location = "/places/"+this.value;
    });

    // $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
    //     e.preventDefault();
    //     var $form=$(this);
    //     $('#confirm').modal({ backdrop: 'static', keyboard: false })
    //         .on('click', '#delete-btn', function(){
    //             $form.submit();
    //         });
    // });

    $('form.form-delete').on('click', function(e){
        e.preventDefault();
        var $form=$(this);
        $('#confirm').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete-btn', function(){
                $form.submit();
            });
    });

    $('.remove_image_branch').click(function(e){
        e.preventDefault();

        var csrf_token = $('input[name="_token"]').val(),
            branch_image_id = this.dataset.id,
            type = this.dataset.type;

        $('#confirm').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete-btn', function(){
                $.ajax({
                    type: "POST",
                    url: '/ajax/destroy-branch-image',
                    data: {id: branch_image_id, _token: csrf_token, type: type},
                    success: function (data) {
                        window.location.reload();
                    }
                });
            });
    });


    $( "#country" ).change(function () {
        var country_id = $(this).val(),
            csrf_token = $('input[name="_token"]').val();

        if (country_id == '')
        {
            var select = document.getElementById('city');
            select.options.length = 0;
            return  select.options.add(new Option('Город',null));
        }

        $('#city').parent().parent().removeClass('hidden');

        $.ajax({
            type: "POST",
            url: '/ajax/cities',
            data: {id: country_id, _token: csrf_token},
            success: function (data) {

                var select = document.getElementById('city');
                select.options.length = 0;

                for (var i in data) {
                    select.options.add(new Option(data[i].name_ru,data[i].id));
                }
            }
        });

        $('#city').removeClass('hidden');
    });

    $( "#place-input" ).change(function () {
        var place_id = $(this).val(),
            csrf_token = $('input[name="_token"]').val();

        if (place_id == '')
        {
            var select = document.getElementById('branch-input');
            select.options.length = 0;
            return  select.options.add(new Option('Не выбрано',null));
        }

        $('#branch-input').parent().parent().removeClass('hidden');

        $.ajax({
            type: "POST",
            url: '/ajax/branches',
            data: {id: place_id, _token: csrf_token},
            success: function (data) {
                var select = document.getElementById('branch-input');
                select.options.length = 0;

                if(data.length) {
                    for (var i in data) {
                        select.options.add(new Option(data[i].name,data[i].id));
                    }
                } else {
                    select.options.add(new Option('Не выбрано',null));
                }
            }
        });

        $('#branch-input').removeClass('hidden');
    });
});