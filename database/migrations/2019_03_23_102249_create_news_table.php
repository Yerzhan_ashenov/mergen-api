<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('alias')->unique();
            $table->text('short_description');
            $table->text('description');
            $table->integer('image_id')->nullable();
            $table->integer('views')->unsigned()->default(0);
            $table->integer('category_id')->nullable();
            $table->boolean('on_main')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
