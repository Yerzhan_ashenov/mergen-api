<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Menu;
use Dosarkz\Lora\Installation\Modules\Lora\Models\MenuItem;
use Dosarkz\Lora\Installation\Modules\Lora\Models\MenuRole;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Role;

class AddBonuseToLora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $menu =   Menu::create([
            'name_ru' => 'Бонусы',
            'name_en'   =>  'Bonuses',
            'alias' =>  'bonuses',
            'type_id' => Menu::TYPE_LEFT_SIDE_MENU,
            'status_id' => 1,
            'position'  => 2,
        ]);

         MenuItem::create([
            'title_ru' => 'Бонусы',
            'title_en'  =>  'Bonuses',
            'url' => '/lora/bonuses',
            'icon' => 'fa-star',
            'position' => 1,
            'menu_id' => $menu->id,
            'status_id' => 1
        ]);

        MenuRole::create([
            'role_id' => Role::where('alias', 'admin')->first()->id,
            'menu_id'   => $menu->id,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menu = Menu::where('alias', 'bonuses')->first();

        if(!$menu)
        {
            return false;
        }

        $menu->menuRoles()->delete();
        $menu->menuItems()->delete();
        $menu->delete();
    }
}
