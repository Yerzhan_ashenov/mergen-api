<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table){
            $table->timestamp('delivered_at')->nullable();
            $table->integer('type_payment')->default(0);
            $table->text('comment')->nullable();
            $table->integer('delivery_type')->nullable();
            $table->text('delivery_address')->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->decimal('available_sum')->nullable();
            $table->integer('promo_code_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table){
            $table->dropColumn('delivered_at');
            $table->dropColumn('type_payment');
            $table->dropColumn('comment');
            $table->dropColumn('delivery_type');
            $table->dropColumn('delivery_address');
            $table->dropColumn('delivery_date');
            $table->dropColumn('available_sum');
            $table->dropColumn('promo_code_id');
        });
    }
}
