<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_codes', function(Blueprint $table){
            $table->decimal('percentage_of_user',10,2)->default(0);
        });

        Schema::table('promo_uses', function(Blueprint $table){
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_codes', function(Blueprint $table){
            $table->dropColumn('percentage_of_user');
        });

        Schema::table('promo_uses', function(Blueprint $table){
            $table->dropForeign(['order_id']);
            $table->dropColumn('order_id');
        });
    }
}
