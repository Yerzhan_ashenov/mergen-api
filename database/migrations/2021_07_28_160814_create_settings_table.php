<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('type_id')->default(0);
            $table->text('value')->nullable();
            $table->integer('status_id')->default(0);
            $table->timestamps();
        });

        \App\Models\Setting::create([
            'name' => 'Процент бонуса',
            'slug' => 'percentage_of_bonus',
            'type_id' => \App\Models\Setting::TYPE_INPUT,
            'value' => '3',
            'status_id' => \App\Models\Setting::STATUS_ACTIVE,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
