<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Menu;
use Dosarkz\Lora\Installation\Modules\Lora\Models\MenuItem;
use Dosarkz\Lora\Installation\Modules\Lora\Models\MenuRole;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Role;


class AddCitiesToLora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $menu =   Menu::create([
            'name_ru' => 'Города',
            'name_en'   =>  'City',
            'alias' =>  'cities',
            'type_id' => Menu::TYPE_LEFT_SIDE_MENU,
            'status_id' => 1,
            'position'  => 2,
        ]);

        $MenuItem =  MenuItem::create([
            'title_ru' => 'Города',
            'title_en'  =>  'City',
            'url' => '/lora/cities',
            'icon' => 'fa-briefcase',
            'position' => 1,
            'menu_id' => $menu->id,
            'status_id' => 1
        ]);

        MenuItem::create([
            'title_ru' => 'Добавить',
            'title_en'  =>  'Add',
            'url' => '/lora/cities/create',
            'icon' => 'fa-plus-circle',
            'menu_id' => $menu->id,
            'parent_id' => $MenuItem->id,
            'position' => 1,
            'status_id' => 1
        ]);

        MenuItem::create([
            'title_ru' => 'Список',
            'title_en'  =>  'List',
            'url' => '/lora/cities',
            'icon' => 'fa-list-ul',
            'menu_id' => $menu->id,
            'parent_id' => $MenuItem->id,
            'position' => 1,
            'status_id' => 1
        ]);

        MenuRole::create([
            'role_id' => Role::where('alias', 'admin')->first()->id,
            'menu_id'   => $menu->id,
        ]);

        $kz =  \App\Models\Country::firstOrCreate([
            'title' => 'Казахстан',
            'status_id' => \App\Models\Country::STATUS_ACTIVE,
            'slug' => 'kazakhstan',
        ]);

        \App\Models\City::firstOrCreate([
            'title' => 'Нур-Султан',
            'status_id' => \App\Models\City::STATUS_ACTIVE,
            'slug' => 'astana',
            'country_id' => $kz->id,
        ]);

        \App\Models\City::firstOrCreate([
            'title' => 'Алматы',
            'status_id' => \App\Models\City::STATUS_ACTIVE,
            'slug' => 'almaty',
            'country_id' => $kz->id,
        ]);

        \App\Models\City::firstOrCreate([
            'title' => 'Кокшетау',
            'status_id' => \App\Models\City::STATUS_ACTIVE,
            'slug' => 'kokshetau',
            'country_id' => $kz->id,
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menu = Menu::where('alias', 'cities')->first();

        if(!$menu)
        {
            return false;
        }

        $menu->menuRoles()->delete();
        $menu->menuItems()->delete();
        $menu->delete();
    }
}
