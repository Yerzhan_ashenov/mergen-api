<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitIdStepsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table){
            $table->integer('unit_id')->nullable();
            $table->decimal('unit_step', 10)->default(1);
            $table->decimal('unit_starting_point', 10)->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table){
            $table->dropColumn('unit_id');
            $table->dropColumn('unit_step');
            $table->dropColumn('unit_starting_point');
        });
    }
}
