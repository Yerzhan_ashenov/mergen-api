<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->nullable();
            $table->unsignedBigInteger('phone')->unique()->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('user_role_id')->nullable();
            $table->string('email')->unique();
            $table->integer('image_id')->nullable();
            $table->text('password');
            $table->string('address')->nullable();
            $table->string('company')->nullable();
            $table->string('api_token', 60)->unique();
            $table->timestamp('api_token_expired_at')->nullable();
            $table->integer('status_id')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
