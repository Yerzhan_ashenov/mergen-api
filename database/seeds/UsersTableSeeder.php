<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phone = 77083600123;
        $admin = \App\Models\User::where('phone', $phone)->first();

        if($admin)
        {
            $admin->update([
                'email' => 'm.zhaxylykov@gmail.com',
                'username' => 'm.zhaxylykov',
                'phone' => '77083600123',
                'password' => bcrypt('123456'),
                'api_token' => str_random(60),
            ]);
        }else{
            $admin =   \App\Models\User::firstOrCreate([
                'phone' => '77083600123',
                'email' => 'm.zhaxylykov@gmail.com',
                'username' => 'm.zhaxylykov',
                'password' => bcrypt('123456'),
                'api_token' => str_random(60),
            ]);
        }

        $user_role = \App\Models\UserRole::firstOrCreate([
            'user_id' => $admin->id,
            'role_id' => \App\Models\Role::ROLE_ADMIN
        ]);

        $admin->update([
            'user_role_id' => $user_role->id,
            'status_id' => \App\Models\User::STATUS_ACTIVE
        ]);

        \App\Models\Unit::firstOrCreate([
            'title' =>  'кг',
            'step'  =>  '0.1',
            'starting_point'    =>  '1'
        ]);

        \App\Models\Unit::firstOrCreate([
            'title' =>  'литр',
            'step'  =>  '1',
            'starting_point'    =>  '2'
        ]);
    }
}
