<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        DB::table('roles')->delete();

        Role::create([
            'name_ru' => 'Администратор',
            'alias' => 'admin',
            'status_id' => Role::STATUS_DEFAULT
        ]);

        Role::create([
            'name_ru' => 'Супер админ',
            'alias' => 'super_admin',
            'status_id' => Role::STATUS_DEFAULT
        ]);

        Role::create([
            'name_ru' => 'Пользователь',
            'alias' => 'user',
            'status_id' => Role::STATUS_DEFAULT
        ]);

    }
}
