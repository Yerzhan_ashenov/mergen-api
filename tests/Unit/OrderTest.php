<?php

namespace Tests\Unit;

use App\Models\City;
use App\Models\Order;
use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPay()
    {
        $user = \App\Models\User::where('phone', '77771112233')->first();
        $order = Order::findOrFail(177);

        $this->json('POST', "/api/v1/orders/$order->id/pay?api_token=".$user->api_token)
         //  ->dump()
            ->assertStatus(200);
    }

    public function testPayStatus()
    {
        $user = \App\Models\User::where('phone', '77771112233')->first();
        $order = Order::findOrFail(177);

        $this->json('GET', "/api/v1/orders/$order->id/pay-status?api_token=".$user->api_token)
         //       ->dump()
            ->assertStatus(200);
    }

    public function testStoreOrder()
    {
        $user = \App\Models\User::where('phone', '77771112233')->first();

        $this->json('POST', "/api/v1/orders?api_token=".$user->api_token,[
            'products' => [
                [
                    'id' =>  Product::first()->id,
                    'cost' => 400,
                    'quantity' => 2,

                ]
            ],
            //'promo_code' => 'JanaJyl',
         //   'bonus' => 1,
            'city_id' => '1',
            'area_id' => 2,
            'phone' => '77056543564',
            'name' => 'Yerz'
        ])
          //  ->dump()
            ->assertStatus(200);
    }

    public function testStoreOrderGuest()
    {
        $this->json('POST', "/api/v1/orders/guest",[
            'products' => [
                [
                    'id' =>  Product::first()->id,
                    'cost' => 400,
                    'quantity' => 2,

                ]
            ],
            //'promo_code' => 'JanaJyl',
            //   'bonus' => 1,
            'city_id' => '1',
            'area_id' => 2,
            'phone' => '77056543564',
            'name' => 'Yerz'
        ])
             // ->dump()
            ->assertStatus(200);
    }

    public function testAreas()
    {
        $user = \App\Models\User::where('phone', '77771112233')->first();
        $city = City::first();
        $this->json('GET', "/api/v1/cities/3/delivery-areas?api_token=".$user->api_token)
            // ->dump()
            ->assertStatus(200);
    }

    public function testGetOrders()
    {
        $user = \App\Models\User::where('phone', '77771112233')->first();
        $this->json('GET', "/api/v1/orders?api_token=".$user->api_token)
             ->dump()
            ->assertStatus(200);
    }
}
