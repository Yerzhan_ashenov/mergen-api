<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    public function testGetProfile()
    {
        $user = \App\Models\User::where('phone', '77771112233')->first();

        $this->json('GET', "/api/v1/profile?api_token=".$user->api_token)
           // ->dump()
            ->assertStatus(200);
    }
}