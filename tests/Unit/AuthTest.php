<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    public function testRegister()
    {
        $response = $this->json('POST', '/api/v1/auth/register', [
            'phone'=> '77056543564'
        ]);

        $response
          //  ->dump()
            ->assertStatus(200);
    }

    public function testConfirmSms()
    {
        $response = $this->json('POST', '/api/v1/auth/confirm-sms', [
            'phone'=> '77056543564',
            'code' => '269335'
        ]);

        $response
          //   ->dump()
            ->assertStatus(200);
    }

    public function testResendSms()
    {
        $response = $this->json('POST', '/api/v1/auth/resend-sms', [
            'phone'=> '77056543564',
        ]);

        $response
              ->dump()
            ->assertStatus(200);
    }
}
