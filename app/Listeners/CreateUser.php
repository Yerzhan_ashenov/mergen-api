<?php

namespace App\Listeners;

use App\Events\UserInvitationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param UserInvitationCreated $event
     */
    public function handle(UserInvitationCreated $event)
    {
        $user =   \App\Models\User::firstOrCreate([
            'phone' => $event->user_invitation->phone,
            'password' => bcrypt($event->user_invitation->code),
            'api_token' => str_random(60),
        ]);

        $user_role = \App\Models\UserRole::firstOrCreate([
            'user_id' => $user->id,
            'role_id' => \App\Models\Role::ROLE_USER
        ]);

        $event->user_invitation->update([
            'invited_user_id' => $user->id,
        ]);

        $user_role->update([
            'user_role_id' => $user_role->id,
            'status_id' => \App\Models\User::STATUS_ACTIVE
        ]);

        $user->update(['user_role_id', $user_role->id]);
    }
}
