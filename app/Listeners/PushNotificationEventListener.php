<?php

namespace App\Listeners;

use App\Events\ChatMessageWasReceived;
use App\Events\UserInvitationCreated;
use App\Models\UserDevice;
use App\Repositories\Notification\Push;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class PushNotificationEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param UserInvitationCreated $event
     */
    public function handle(ChatMessageWasReceived $event)
    {
        $users = [];

        foreach ($event->message->recipients as $recipient) {
            $users[] = $recipient->user_id;
        }

        $devices = UserDevice::whereIn('user_id',[implode(',', $users)])->get();

        $push = new Push('У вас новое сообщение', 'Описание', $event->message, $devices);
        $push->send();
    }
}
