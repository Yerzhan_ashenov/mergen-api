<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public $table = 'users';
    const STATUS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name','last_name', 'middle_name', 'company', 'address', 'city_id', 'user_role',
        'api_token', 'phone', 'email', 'status_id', 'password', 'user_role_id', 'update_at'
    ];

    protected $visible = [
        'id', 'name', 'first_name','last_name', 'middle_name', 'company', 'address', 'user_role',
        'phone', 'email'
    ];

    protected $appends = ['user_role'];

    public function getUserRoleAttribute()
    {
        $user_role = UserRole::find($this->user_role_id);
        return $user_role ? $user_role : null;
    }

    public function userRole()
    {
        return $this->belongsTo(UserRole::class, 'id', 'user_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' .$this->last_name;
    }
}
