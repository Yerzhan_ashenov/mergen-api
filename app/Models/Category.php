<?php

namespace App\Models;

use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    const STATUS_ACTIVE = 1;
    /**
     * @var array
     */
    protected $fillable = ['title','alias','parent_id','status_id','image_id','icon', 'position', 'local_id'];
    /**
     * @var array
     */
    protected $visible = ['id','title','alias','image', 'subs', 'local_id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    public $appends =  ['subs'];

    public function getSubsAttribute()
    {
        if ($subs = self::where('status_id', true)->where('parent_id', $this->id)->orderBy('position','ASC')->get())
        {
            return $subs;
        }
        return null;
    }

    public static function parents()
    {
        return self::where('status_id',self::STATUS_ACTIVE)
            ->where('parent_id',0)
            ->orderBy('position')
            ->get();
    }

    public function subs()
    {
        return $this->hasMany($this,'parent_id','id')->orderBy('position','ASC');
    }

    public static function parentsList($model)
    {
        $result = [];
        $parents = self::where('status_id',1)
            ->select('id', 'title')
            ->where('parent_id',0)
            ->orderBy('position')
            ->get();

        foreach($parents as $parent) {
            if($model->id == $parent->id)
            {
                continue;
            }

            $result[$parent->id] = $parent->title;
        }

        return $result;
    }

    public static function maxPosition()
    {
        $maxPosition = self::select(DB::raw('MAX(position) AS position'))
            ->first();
        return ($maxPosition && isset($maxPosition->position)) ? $maxPosition->position + 1 : 0;
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentCategory()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable')->orderBy('created_at','desc');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function scopeFilter($query)
    {
        if(request()->has('q'))
        {
            $q = request()->input('q');
            $query->where('title', 'like', "%$q%");
        }
    }

}
