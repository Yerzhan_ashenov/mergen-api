<?php

namespace App\Models;

use App\Classes\Menu\RoleMenu;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Date\Date;

class User extends Authenticatable
{
    use Notifiable;

    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'middle_name', 'company', 'address', 'city_id', 'user_role',
        'api_token', 'phone', 'email', 'status_id', 'password', 'user_role_id', 'update_at'
    ];

    protected $visible = [
        'id', 'username', 'first_name', 'last_name', 'middle_name', 'company', 'address', 'city_id', 'user_role',
        'api_token', 'phone', 'email', 'status_id', 'created_at', 'updated_at', 'bonus'
    ];

    protected $appends = ['user_role', 'bonus'];


    public function hasRole($role)
    {
        return (boolean)UserRole::whereHas('role', function ($query) use ($role) {
            $query->where('alias', $role);
        })
            ->where('user_id', $this->id)
            ->first();
    }

    public function getBonusAttribute()
    {
        return floor($this->userBonuses->sum('amount'));
    }

    public function userBonuses()
    {
        return $this->hasMany(UserBonus::class, 'user_id')->where('status_id',
            UserBonus::STATUS_ACTIVE);
    }

    public function getMenuAttribute()
    {
        $menu = new RoleMenu($this->userRole->role);
        return $menu->render();
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserRoleAttribute()
    {
        $user_role = UserRole::find($this->user_role_id);
        return $user_role ? $user_role : null;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name . ' ' . $this->middle_name;
    }

    public function userRole()
    {
        return $this->belongsTo(UserRole::class, 'id', 'user_id');
    }

    public function getCreatedDateAttribute()
    {
        Date::setLocale('ru');
        return Date::parse($this->created_at . " + 6 hour", 'Asia/Almaty')->format('j F Y H:i:s');
    }

    public function scopeFilter($query)
    {
        $phone = request()->input('phone');
        if (isset($phone)) {
            $query->where('phone', 'like', "%$phone %");
        }
        return $query;
    }

    public function routeNotificationForSmscru()
    {
        return $this->phone;
    }

    public function userSms()
    {
        return $this->belongsTo(UserSms::class, 'phone', 'phone')
            ->where('status_id', UserSms::STATUS_CREATED);
    }
}
