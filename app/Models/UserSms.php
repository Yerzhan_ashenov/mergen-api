<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserSms extends Model
{
    const STATUS_CREATED = 0;
    const STATUS_USED = 1;

    protected $fillable = [
        'phone', 'code', 'status_id', 'tries'
    ];


}