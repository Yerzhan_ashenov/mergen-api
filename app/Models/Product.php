<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;

class Product extends Model
{
    const STATUS_ACTIVE = 1;
    /**
     * @var array
     */
    protected $fillable = ['title', 'user_id', 'description','alias','unit_id', 'cost','category_id',
        'status_id','image_id', 'position', 'unit_starting_point', 'unit_step'];
    /**
     * @var array
     */
    protected $visible = ['id', 'title', 'user', 'description','alias','unit', 'cost','category_id','unit_id',
        'status_id','image', 'position', 'unit_starting_point', 'unit_step'];

    /**
     * @var bool
     */
    public $timestamps = true;

    public $appends = ['user', 'unit'];

    public function getUserAttribute()
    {
        return Person::find($this->user_id);
    }

    public function getUnitAttribute()
    {
        $unit = Unit::find($this->unit_id);
        return $unit ? $unit->title : null;
    }


    public function getListCategoriesAttribute()
    {
        return HierarchyCollector::getHierarchyList();
    }

    public function getUnitListAttribute()
    {
        return Unit::pluck('title','id');
    }

    public function scopeFilter($query)
    {
        if(request()->has('category_id') && request()->input('category_id') != '')
        {
            $query->where('category_id', request()->input('category_id'));
        }

        $this->filterByName($query);
    }

    public function filterByName(&$query)
    {
        $q = request()->input('q');

        if(isset($q))
        {
            $query->where('title', 'like', "%$q%")
                ->orWhere('description', 'like', "%$q%");
        }

    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable')->orderBy('created_at','desc');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getTagListAttribute()
    {
        return implode(',', Tag::where('product_id',$this->id)->pluck('title')->toArray());
    }

    public function tags()
    {
        return $this->hasMany(Tag::class, 'product_id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

}
