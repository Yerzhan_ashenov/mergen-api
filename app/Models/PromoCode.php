<?php
/**
 * Created by PhpStorm.
 * User: dosarkz
 * Date: 2019-03-23
 * Time: 17:03
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    const STATUS_FREE = 0;
    const STATUS_USED = 1;

    protected $fillable = [
        'title', 'expired_at', 'cost', 'min_amount', 'status_id', 'token', 'user_id', 'percentage_of_user'
    ];

    public $timestamps = true;
    public $dates = ['expired_at'];

    public function getUsedAttribute()
    {
        return $this->promoUses->count();
    }

    public function promoUses()
    {
        return $this->hasMany(PromoUse::class, 'promo_id');
    }

}