<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $fillable = [
        'user_id', 'role_id'
    ];
    protected  $visible = [
        'id', 'user_id', 'role', 'created_at','updated_at'
    ];

    protected $appends = ['role'];

    public function getRoleAttribute()
    {
        $role = Role::find($this->role_id);
        return $role ? $role : null;
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public $timestamps = true;
}
