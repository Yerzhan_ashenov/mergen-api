<?php
/**
 * Created by PhpStorm.
 * User: dosarkz
 * Date: 2019-03-22
 * Time: 01:41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
      'title', 'product_id'
    ];

    public $timestamps = true;
}