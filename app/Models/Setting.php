<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const TYPE_INPUT = 0;
    const TYPE_SELECT = 1;

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLE = 0;

    protected $fillable = ['name', 'slug', 'type_id', 'value', 'status_id'];

    public function scopeActive($query)
    {
        $query->where('status_id', self::STATUS_ACTIVE);
        return $query;
    }
}