<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Role as BasicRole;

class Role extends BasicRole
{
    const ROLE_ADMIN = 1;
    const ROLE_SUPER_ADMIN = 2;
    const ROLE_USER = 3;

    protected $visible = [
        'id','alias', 'title'
    ];

    public function getTitleAttribute()
    {
        return $this->name_ru;
    }

    /**
     * @var bool
     */
    public $timestamps = true;

}