<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVATED = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'country_id', 'status_id'
    ];

    public $timestamps = true;

    public function getStatusAttribute()
    {
        return $this->statuses[$this->status_id];
    }

    public function getStatusesAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DEACTIVATED => 'Не активен'
        ];
    }

    public function getCountriesAttribute()
    {
        return Country::pluck('title', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status_id', self::STATUS_ACTIVE);
    }

}
