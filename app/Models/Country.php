<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVATED = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'status_id', 'slug'
    ];

    public $timestamps = true;

    public function getStatusAttribute()
    {
        return $this->statuses()[$this->status_id];
    }

    private function statuses()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DEACTIVATED => 'Не активен'
        ];
    }

}
