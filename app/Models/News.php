<?php
namespace App\Models;

use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    const STATUS_ACTIVE = 1;

    protected $table = 'news';
    protected $fillable = ['title', 'alias', 'short_description', 'description', 'image_id', 'created_at', 'status_id',
        'on_main'];

    protected $visible = ['id', 'title', 'alias', 'short_description', 'description', 'created_at', 'status_id',
        'on_main', 'image'];


    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d',strtotime($value));
    }

    public function image()
    {
        return $this->morphOne(Image::class,'imageable')->orderBy('created_at','desc');
    }

    public function images()
    {
        return $this->morphMany(Image::class,'imageable');
    }



}