<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Psy\Util\Str;

class Order extends Model
{
    const STATUS_CREATED = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_REJECTED = 4;

    const DELIVERY_TYPE_COURIER = 1;

    const TYPE_PAYMENT_CASH = 1;
    const TYPE_PAYMENT_CARD = 2;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'amount', 'position', 'status_id', 'delivered_at', 'type_payment', 'comment',
        'delivery_type', 'delivery_address', 'delivery_date', 'available_sum', 'promo_code_id', 'is_paid', 'bank_order_id',
        'x_idempotency_key', 'city_id', 'area_id', 'name', 'phone'];

    /**
     * @var array
     */
    protected $visible = ['id', 'user_id', 'amount', 'status_id', 'order_items', 'created_at', 'updated_at',
        'is_paid', 'bank_order_id', 'x_idempotency_key', 'city_id', 'area_id', 'delivery_sum', 'name', 'phone'];

    /**
     * @var bool
     */
    public $timestamps = true;
    public $appends = ['order_items', 'delivery_sum'];

    public function getOrderItemsAttribute()
    {
        return OrderItem::where('order_id', $this->id)->get();
    }

    public function getDeliverySumAttribute()
    {
        return $this->area->cost ?? 0;
    }

    public function area()
    {
        return $this->belongsTo(DeliveryArea::class, 'area_id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }

    public function getStatusesAttribute()
    {
        return [
            self::STATUS_CREATED => 'Создан',
            self::STATUS_CONFIRMED => 'Подтвержден',
            self::STATUS_COMPLETED => 'Завершен',
            self::STATUS_REJECTED => 'Отменен'
        ];
    }


    public function getListDeliveriesAttribute()
    {
        return [
            self::DELIVERY_TYPE_COURIER => 'Курьером'
        ];
    }

    public function getDeliveryAttribute()
    {
        return $this->delivery_type ? $this->listDeliveries[$this->delivery_type] : null;
    }

    public function getStatusAttribute()
    {
        return $this->statuses[$this->status_id];
    }

    public static function getTypePaymentsAttribute()
    {
        return [
            self::TYPE_PAYMENT_CASH => 'Наличные',
            self::TYPE_PAYMENT_CARD => 'Картой'
        ];
    }

    public function getPaymentAttribute()
    {
        return $this->type_payment ? $this->typePayments[$this->type_payment] : null;
    }

    public function getListPersonsAttribute()
    {
        return Person::get()->mapWithKeys(function ($item) {

            if (!$item['first_name'] && !$item['last_name']) {
                $username = $item['phone'];
            } else {
                $username = $item['first_name'] . ' ' . $item['last_name'];
            }

            return [$item['id'] => $username];
        });
    }

    public function user()
    {
        return $this->belongsTo(Person::class, 'user_id');
    }

    public function getCreatedDateAttribute()
    {
        Date::setLocale('ru');
        return Date::parse($this->created_at . " + 6 hour", 'Asia/Almaty')
            ->format('j F Y H:i:s');
    }

    public function getTotalAttribute()
    {
        $total = 0;
        $total += $this->orderItems->sum('sum');

        if ($this->promoUse) {
            $total = $total - ($total * $this->promoUse->promo->cost / 100);
        }

        if ($this->area) {
            $total += $this->area->cost;
        }

        return $total ?? 0;
    }

    public function promoUse()
    {
        return $this->hasOne(PromoUse::class, 'order_id');
    }

    public function getUUIDAttribute()
    {
        if (!$this->x_idempotency_key) {
            $this->update([
                'x_idempotency_key' => \Illuminate\Support\Str::uuid(),
            ]);
        }

        return $this->x_idempotency_key;
    }

}
