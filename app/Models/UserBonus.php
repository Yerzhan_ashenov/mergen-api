<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserBonus extends Model
{
    CONST STATUS_ACTIVE = 0;
    CONST STATUS_USED = 1;

    protected $fillable = [
        'user_id', 'order_id', 'amount', 'status_id'
    ];

}