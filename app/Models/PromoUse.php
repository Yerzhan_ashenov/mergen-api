<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoUse extends Model
{
    protected $table = 'promo_uses';
    protected $fillable = ['promo_id', 'user_id', 'order_id'];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function promo()
    {
        return $this->belongsTo(PromoCode::class, 'promo_id');
    }
}