<?php
/**
 * Created by PhpStorm.
 * User: noname
 * Date: 10/25/19
 * Time: 14:40
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DeliveryArea extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLE = 0;

    protected $fillable = [
        'title','cost','status_id', 'city_id'
    ];

    protected $visible = [
        'id', 'title', 'cost', 'city_id'
    ];

    public $timestamps = true;

    public function getStatusesAttribute()
    {
        return [
            self::STATUS_DISABLE => 'Отключен',
            self::STATUS_ACTIVE => 'Активен',
        ];
    }

    public function getStatusAttribute()
    {
        return $this->statuses[$this->status_id];
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}