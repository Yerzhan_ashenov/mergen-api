<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    const STATUS_ACTIVE = 1;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'order_id','product_id', 'sum','position', 'status_id', 'quantity'];
    /**
     * @var array
     */
    protected $visible = ['id','user_id', 'order_id','product', 'sum','position', 'status_id', 'quantity'];

    /**
     * @var bool
     */
    public $timestamps = true;
    public $appends = ['product'];

    public function getProductAttribute()
    {
        return Product::with('image')->whereId($this->product_id)->first();
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function getScoreAttribute()
    {
        return $this->sum * $this->quantity;
    }


}
