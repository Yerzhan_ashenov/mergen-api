<?php
/**
 * Created by PhpStorm.
 * User: noname
 * Date: 03/20/19
 * Time: 17:43
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'title', 'starting_point', 'step'
    ];

    public $timestamps = true;
}