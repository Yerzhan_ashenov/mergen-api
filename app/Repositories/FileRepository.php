<?php
namespace App\Repositories;

use App\Repositories\File\NewsImageUploader;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Intervention\Image\Image as ImageModel;

class FileRepository
{
    public function upload($images, $branch_id)
    {
           try{
               $array = null;

               foreach ($images as $image) {

               $filename = time() . '_' . rand(0,100) . '.' . $image->getClientOriginalExtension();
               $thumb = 'thumb_' . $filename;

               Image::make($image->getRealPath())->save(public_path('images/gallery/' . $filename));
               Image::make($image->getRealPath())
                   ->resize(150, null, function ($constraint) {
                       $constraint->aspectRatio();
                   })
                   ->save(public_path('images/gallery/' . $thumb));

                   $new_image = ImageModel::create([
                       'path' => 'images/gallery/',
                       'name' => $filename,
                       'thumb' => $thumb,
                       'published_at' => new \DateTime()
                   ]);

                   $array[]  =  BranchImage::create([
                       'branch_id' => $branch_id,
                       'image_id'  => $new_image->id
                   ])->toArray();

               }

               return $array;

           }catch (\Exception $e)
           {
               return $e->getMessage();
           }
    }

    public function uploadImageNews($image)
    {
        $newsImageUploader = new NewsImageUploader($image);
           return $newsImageUploader->upload();
    }


}