<?php
namespace App\Repositories;

use App\Models\PromoCode;
use App\Models\PromoUse;
use App\Models\Setting;
use App\Models\User;
use App\Models\UserBonus;
use App\Models\UserDevice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function create($request)
    {
        $user = User::create([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'api_token' => $this->generateHash(),
            'api_token_expired_at' => Carbon::createFromDate()->addDay(1),
            'phone_number' => $request->input('phone_number'),
            'status_id' => 1,
        ]);

        $device = UserDevice::where('uuid', $request->input('uuid'))->first();

        if ($device)
        {
            // if user not equal device user if reset push token
            if ($device->user_id != $user->id)
            {
                $device->user_id = $user->id;
                $device->push_token = null;
                $device->save();
            }

        }else{
            UserDevice::create([
                'user_id' => $user->id,
                'uuid' => $request->input('uuid'),
                'model' => $request->input('model'),
                'platform' => $request->input('platform'),
                'version' => $request->input('version'),
            ]);
        }

        return $user;
    }

    public function generateHash()
    {
        return Hash::make(md5(config('app.APP_KEY').'_token_'.md5(time())));
    }

    public function promo()
    {
        $promo = PromoCode::whereTitle($request->input('promo_code'))
            ->where('expired_at', '>=', Carbon::now('Asia/Almaty'))
            ->first();
        $bonus = Setting::where('slug', 'percentage_of_bonus')->active()->first();

        if ($promo) {
            $amount = $amount - ($amount * $promo->cost / 100);
            $promo->update([
                'status_id' => PromoCode::STATUS_USED
            ]);
            PromoUse::create([
                'user_id' => $request->user()->id,
                'promo_id' => $promo->id,
                'order_id' => $order->id,
            ]);
            // add bonus to owner of promo
            if ($promo->user_id && $request->user('api')->id != $promo->user_id && $bonus) {
                UserBonus::create([
                    'order_id' => $order->id,
                    'user_id' => $promo->user_id,
                    'amount' => $amount * $promo->percentage_of_user / 100
                ]);
            }
        } else {
            if ($bonus) {
                $userBonus = $request->user('api')->bonus;
                // use bonus
                if ($request->has('bonus') && $request->input('bonus') == true && $userBonus > 0) {
                    $amount = $amount - $userBonus;

                    $request->user('api')->userBonuses()->update([
                        'status_id' => UserBonus::STATUS_USED
                    ]);
                } else {
                    UserBonus::create([
                        'order_id' => $order->id,
                        'user_id' => $request->user('api')->id,
                        'amount' => $amount * $bonus->value / 100
                    ]);
                }
            }
        }
    }

}