<?php
namespace App\Repositories\File;

use Illuminate\Support\Facades\File;

class PlaceUploader extends Uploader
{
    public function __construct($image)
    {
        $this->image = $image;
        $this->destination = 'images/places/';

        if (!is_dir(public_path($this->destination)))
        {
            File::makeDirectory(public_path($this->destination), $mode = 0777, true);
        }
    }
    /**
     * @return mixed
     */
    public function upload()
    {
        $thumb = $this->getThumb();
        return $this->destination . $this->uploadThumb($thumb);
    }

}