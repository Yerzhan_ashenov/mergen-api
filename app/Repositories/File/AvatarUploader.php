<?php
namespace App\Repositories\File;

class AvatarUploader extends Uploader
{
    public function __construct($image)
    {
        $this->image = $image;
        $this->destination = 'images/avatars/';
    }
    /**
     * @return mixed
     */
    public function upload()
    {
        $thumb = $this->getThumb();
        return $this->destination . $this->uploadThumb($thumb);
    }

}