<?php
namespace App\Repositories\File;

use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;

class NewsImageUploader extends Uploader
{
    public function __construct($image)
    {
        $this->image = $image;
        $this->destination = 'images/news/';
    }

    public function upload()
    {
        $filename = $this->getFileName();
        $thumb = $this->getThumb();

        $this->uploadFile($filename);
        $this->uploadThumb($thumb);

        return $this->createImage($filename, $thumb);
    }

    public function createImage($filename, $thumb)
    {
        return Image::create([
            'path' => $this->destination,
            'name' => $filename,
            'thumb' => $thumb,
            'published_at' => new \DateTime()
        ]);
    }

}