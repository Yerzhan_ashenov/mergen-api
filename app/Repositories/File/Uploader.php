<?php
namespace App\Repositories\File;

use Intervention\Image\Facades\Image;

abstract class Uploader
{
    /**
     * @return mixed
     */
    abstract  function upload();

    /**
     * @var
     */
    protected $fileName;
    /**
     * @var
     */
    protected $thumb;
    /**
     * @var
     */
    protected $image;
    /**
     * @var
     */
    protected $destination;

    /**
     * @return string
     */
    public function getFileName()
    {
        return time() . '_' . rand(0,100) . '.' . $this->image->getClientOriginalExtension();
    }

    /**
     * @return string
     */
    public function getThumb()
    {
        return 'thumb_' . $this->getFileName();
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function uploadFile($filename)
    {
        return Image::make($this->image->getRealPath())->save(public_path($this->destination . $filename));
    }

    /**
     * @param $thumb
     * @return mixed
     */
    public function uploadThumb($thumb)
    {
       Image::make($this->image->getRealPath())->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(public_path($this->destination . $thumb));

        return $thumb;
    }
}