<?php
namespace App\Repositories;

use Illuminate\Http\JsonResponse;

/**
 * Class Response
 * @package App\Classes
 */
class ResponseRepository
{
    /**
     * @var bool
     */
    public $status = false;
    /**
     * @var string
     */
    public $content = null;
    /**
     * @var string
     */
    public $errors = [];
    /**
     * @var string
     */
    public $messages = [];


    public function __construct($status = false, $content = null, array $errors = [], array $messages = [])
    {
        $this->getStatus($status);
        $this->getContent($content);
        $this->getErrors($errors);
        $this->messages = $messages;
    }

    /**
     * @param $status_code
     * @param $content
     * @param $errors
     * @param $messages
     * @return JsonResponse
     */
    public function get($status_code, $content = null, array $errors = [], array $messages = [])
    {
        return response()->json(new self($status_code, $content, $errors, $messages), $status_code);
    }

    /**
     * @param $errors
     */
    protected  function getErrors($errors)
    {
        if ((int)$this->status === true){
            unset($this->errors);
        }else{
            $this->errors = $errors;
        }
    }

    /**
     * @param $status
     */
    protected function getStatus($status)
    {
        $this->status = (int)$status === 200 ? true : false;
    }

    /**
     * @param $content
     */
    public function getContent($content)
    {
        $this->content = $content;
    }


}
