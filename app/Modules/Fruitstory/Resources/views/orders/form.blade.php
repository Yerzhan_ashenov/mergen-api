
@if($model->exists)
    {{ Form::open(array('url' => secure_url_env(route('lora.orders.update', $model->id)),'method'=> 'PUT', 'name'=>'updateCategory', 'files' => true,
     'class' => 'form-horizontal'))}}
@else
    {{ Form::open(array('url' => secure_url_env(route('lora.orders.store')),'method'=> 'POST', 'name'=>'postCategory', 'files' => true, 'class' => 'form-horizontal'))}}
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Пользователь</label>

    <div class="col-md-9">
        {{Form::select('user_id', $model->listPersons, $model->user_id, ['class' => 'form-control', 'id' => 'category-name',
        'placeholder' => 'choose']) }}

        @if ($errors->has('user_id'))
            <span class="help-block">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Способ оплаты</label>

    <div class="col-md-9">
        {{Form::select('type_payment', $model->typePayments, $model->type_payment, ['class' => 'form-control', 'id' => 'category-name',
        'placeholder' => 'choose']) }}

        @if ($errors->has('payment_type'))
            <span class="help-block">
                <strong>{{ $errors->first('payment_type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="title">Статус</label>
    <div class="col-md-9">
    {{Form::select('status_id', $model->statuses, $model->status_id,['class'   => 'form-control'])}}
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">
            @if($model->exists)Обновить@else Создать @endif
        </button>
    </div>
</div>
{{Form::close()}}
