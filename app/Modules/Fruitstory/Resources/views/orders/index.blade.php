@extends('lora::layouts.app')

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <h3>Список заказов</h3>
        </div>

        <div class="card-body">
            <table class="table table-hover" data-form="deleteForm">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Клиент</th>
                    <th>Товары</th>
                    <th>Сумма</th>
                    <th>Доставка</th>
                    <th>Комментарии</th>
                    <th>Оплачено</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @foreach($models as $model)
                    <tr>
                        <td>{{$model->id}}</td>
                        <td>
                            <p>{{$model->user->fullName ?? 'Не указано'}}</p>
                            <p>{{$model->user->phone ?? 'Не указано'}}</p>
                            <p>{{$model->createdDate ?? 'Не указано'}}</p>
                            <p>Cпособ оплаты: {{$model->payment}}</p>
                        </td>
                        <td>
                            <p>Заказано товаров: {{$model->orderItems->count()}}</p>
                            <a href="{{route('lora.orders.items.index', $model->id)}}">Смотреть</a>
                        </td>
                        <td><span>{{$model->total}} тг.</span></td>
                        <td>
                            <p>Тип: {{$model->delivery ?? 'Не указано'}}</p>
                            <p>Адрес: {{$model->delivery_address ?? 'Не указано'}}</p>
                            <p>Дата: {{$model->delivery_date ?? 'Не указано'}}</p>
                            <p>Цена: {{$model->area->cost ?? 'Не указано'}}</p>
                        </td>
                        <td>{{$model->comment ?? 'Не указано'}}</td>
                        <td>{{$model->is_paid ? 'ДА' : 'НЕТ'}}</td>
                        <td>{{$model->status}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('lora.orders.edit', $model->id)}}">
                                <i class="fas fa-edit" aria-hidden="true"></i></a>

                            <button class="btn btn-sm btn-danger delete" type="button"
                                    data-target="#confirm"
                                    data-toggle="modal" data-action="{{route('lora.orders.destroy', $model->id)}}">
                                <i class="fas fa-times" aria-hidden="true"></i></button>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-10">
        <div class="form-group">
            {{ $models->links() }}
        </div>
    </div>
    @include('lora::modals.base_modal')
@endsection

@section('js-append')

    <script>
        $('#confirm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            modal.find('#removeForm').attr('action', button.data('action'))
        })
    </script>
@endsection