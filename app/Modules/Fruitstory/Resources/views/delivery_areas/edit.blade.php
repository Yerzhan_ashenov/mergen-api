@extends('lora::layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Редактировать район</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::delivery_areas.form')
        </div>
    </div>
@endsection