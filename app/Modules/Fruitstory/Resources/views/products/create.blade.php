@extends('lora::layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Создать новую продукцию</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::products.form')
        </div>
    </div>

@endsection


@section('js-append')
    <script src="/js/taginput/bootstrap-tagsinput.min.js"></script>

    <script>
        $('#tags-input').on('itemAdded', function(event) {
            $("#tags").attr('value', $("#tags-input").val());
        });

        $('#tags-input').on('itemRemoved', function(event) {
            $("#tags").attr('value', $("#tags-input").val());
        });

    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="/js/taginput/bootstrap-tagsinput.css">
@endsection