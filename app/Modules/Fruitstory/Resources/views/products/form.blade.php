
@if($model->exists)
    {{ Form::open(array('url' => secure_url_env(route('lora.products.update', $model->id)),'method'=> 'POST', 'name'=>'updateCategory', 'files' => true,
     'class' => 'form-horizontal', 'onkeypress'   =>  'return event.keyCode != 13;'))}}
@else
    {{ Form::open(array('url' => secure_url_env(route('lora.products.store')),'method'=> 'POST', 'name'=>'postCategory',
    'files' => true, 'class' => 'form-horizontal', 'onkeypress'   =>  'return event.keyCode != 13;'))}}
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Название</label>

    <div class="col-md-9">

        {{Form::text('title', $model->title, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Описание</label>

    <div class="col-md-9">

        {{Form::textarea('description', $model->description, ['class' => 'form-control', 'id' => 'product-description']) }}

        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image_id') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Фото</label>

    <div class="col-md-9">

        {{Form::file('image_id', ['class' => 'form-control', 'id' => 'product-image_id']) }}

        @if ($errors->has('image_id'))
            <span class="help-block">
                <strong>{{ $errors->first('image_id') }}</strong>
            </span>
        @endif
        @if($model->image)
            <div class="form-group mt-5">
                <img class="img-thumbnail " src="/{{$model->image->getThumb()}}" width="150" height="150" alt="">
            </div>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('unit_id') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Ед. измерения</label>

    <div class="col-md-9">

        {{Form::select('unit_id', $model->unitList, $model->unit_id, ['class' => 'form-control', 'id' => 'product-unit',
        'placeholder'   =>  'Выберите ед']) }}

        @if ($errors->has('unit_id'))
            <span class="help-block">
                <strong>{{ $errors->first('unit_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('unit_starting_point') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Начальное значение</label>

    <div class="col-md-9">

        {{Form::text('unit_starting_point', $model->unit_starting_point, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('unit_starting_point'))
            <span class="help-block">
                <strong>{{ $errors->first('unit_starting_point') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('unit_step') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Шаг</label>

    <div class="col-md-9">

        {{Form::text('unit_step', $model->unit_step, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('unit_step'))
            <span class="help-block">
                <strong>{{ $errors->first('unit_step') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Цена</label>

    <div class="col-md-9">

        {{Form::number('cost',$model->cost, ['class' => 'form-control', 'id' => 'product-cost']) }}

        @if ($errors->has('cost'))
            <span class="help-block">
                <strong>{{ $errors->first('cost') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Категория</label>

    <div class="col-md-9">

        {{Form::select('category_id', $model->listCategories,$model->category_id, ['class' => 'form-control', 'id' => 'product-category_id',
        'placeholder' => 'Не выбрана']) }}

        @if ($errors->has('category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
    <label for="position" class="col-md-2 control-label">Позиция</label>

    <div class="col-md-9">

        {{Form::number('position', $model->exists ? $model->position : \App\Models\Category::maxPosition(),
        ['class' => 'form-control']) }}

        @if ($errors->has('position'))
            <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="title">Статус</label>
    <div class="col-md-9">
    {{Form::select('status_id', [1=> 'Активен', 0 => 'Не активен'], $model->status_id,['class'   => 'form-control', 'placeholder' =>
    'Не выбран'])}}
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label" for="title">Тэги</label>
    <div class="col-md-9">
        {{Form::text('tags', $model->taglist,['data-role'    =>  'tagsinput', 'id'   =>  'tags-input' ])}}
        {{Form::hidden('tags',null, ['id'  =>  'tags'])}}
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">
            @if($model->exists)Обновить@else Создать @endif
        </button>
    </div>
</div>
{{Form::close()}}
