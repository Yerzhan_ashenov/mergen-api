@extends('lora::layouts.app')

@section('content')
        <div class="card card-blue">
            <div class="card-body">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form" role="form" method="get">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="q">Поиск по названию</label>
                                    <input type="text" name="q" class="form-control input-sm" id="q" value="{{request('q')}}">
                                </div>

                                <div class="form-group col-md-3">
                                    <label class="control-label" for="category_id">Категория</label>

                                    {{Form::select('category_id', $categories, request()->input('category_id'),
['class' => 'form-control product-category_id', 'id' => 'product-category_id','placeholder' => 'Не выбрана']) }}

                                </div>

{{--                                <div class="form-group col-md-3">--}}
{{--                                    <label class="control-label" for="id">ID</label>--}}
{{--                                    <input type="text" name="id" class="form-control input-sm" id="id" value="{{request('id')}}">--}}
{{--                                </div>--}}
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-warning filter-col">
                                    <span class="glyphicon glyphicon-record"></span> Найти
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    <div class="card card-default">
        <div class="card-header">
            <h3>Список Продукции</h3>
        </div>

        <div class="card-body">
            <table class="table table-hover" data-form="deleteForm">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Статус</th>
                    <th>Позиция</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($models as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->category ? $item->category->title : null}}</td>
                        <td>{{$item->status_id == 1 ? 'Активный' : 'Не активен'}}</td>
                        <td>{{$item->position}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('lora.products.edit', $item->id)}}">
                                <i class="fas fa-edit" aria-hidden="true"></i></a>

                            <button class="btn btn-sm btn-danger delete" type="button"
                                    data-target="#confirm"
                                    data-toggle="modal" data-action="{{route('lora.products.destroy', $item->id)}}">
                                <i class="fas fa-times" aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <div class="col-md-10">
                <div class="form-group">
                    {{ $models->appends(request()->except('page'))->links() }}
                </div>
            </div>
            @include('lora::modals.base_modal')
        </div>
    </div>
@endsection
@section('js-append')
    <script src="/vendor/admin/adminlte/plugins/select2/js/select2.js"></script>
    <script>
        $('#confirm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            modal.find('#removeForm').attr('action', button.data('action'))
        })
        $(document).ready(function() {
            $('.product-category_id').select2();
        });

    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="/vendor/admin/adminlte/plugins/select2/css/select2.css">
    <link rel="stylesheet" href="/vendor/admin/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css">
@endsection