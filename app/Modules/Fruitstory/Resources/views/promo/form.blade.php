
@if($model->exists)
    {{ Form::open(array('url' => secure_url_env(route('lora.promo.update', $model->id)),'method'=> 'PUT', 'name'=>'updateCategory', 'files' => true,
     'class' => 'form-horizontal'))}}
@else
    {{ Form::open(array('url' => secure_url_env(route('lora.promo.store')),'method'=> 'POST', 'name'=>'postCategory', 'files' => true, 'class' => 'form-horizontal'))}}
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Код</label>

    <div class="col-md-9">

        {{Form::text('title', $model->title,
        ['class' => 'form-control']) }}

        @if ($errors->has('title'))
            <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
        @endif
    </div>
</div>



<div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
    <label for="position" class="col-md-2 control-label">Сумма скидки</label>

    <div class="col-md-9">

        {{Form::number('cost', $model->cost,
        ['class' => 'form-control']) }}

        @if ($errors->has('cost'))
            <span class="help-block">
            <strong>{{ $errors->first('cost') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('expired_at') ? ' has-error' : '' }}">
    <label for="position" class="col-md-2 control-label">Срок действия промокода</label>

    <div class="col-md-9">

        {{Form::date('expired_at', date('Y-m-d',strtotime($model->expired_at)),
        ['class' => 'form-control']) }}

        @if ($errors->has('expired_at'))
            <span class="help-block">
                <strong>{{ $errors->first('expired_at') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('min_amount') ? ' has-error' : '' }}">
    <label for="min_amount" class="col-md-2 control-label">Сумма заказа ( больше или равно)</label>

    <div class="col-md-9">

        {{Form::number('min_amount', $model->min_amount,
        ['class' => 'form-control']) }}

        @if ($errors->has('min_amount'))
            <span class="help-block">
            <strong>{{ $errors->first('min_amount') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
    <label for="min_amount" class="col-md-4 control-label">Партнер</label>

    <div class="col-md-9">

        {{Form::select('user_id', $users,$model->user_id,['class'   => 'form-control', 'placeholder' => 'Не выбрано'])}}

        @if ($errors->has('user_id'))
            <span class="help-block">
            <strong>{{ $errors->first('user_id') }}</strong>
        </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('percentage_of_user') ? ' has-error' : '' }}">
    <label for="min_amount" class="col-md-4 control-label">Бонус для партнера</label>

    <div class="col-md-9">
        {{Form::number('percentage_of_user', $model->percentage_of_user, ['class' => 'form-control']) }}

        @if ($errors->has('percentage_of_user'))
            <span class="help-block">
            <strong>{{ $errors->first('percentage_of_user') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">
            @if($model->exists)Обновить@else Создать @endif
        </button>
    </div>
</div>
{{Form::close()}}
