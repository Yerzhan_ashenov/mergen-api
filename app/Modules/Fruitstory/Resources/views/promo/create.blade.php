@extends('lora::layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Создать новый код</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::promo.form')
        </div>
    </div>
@endsection