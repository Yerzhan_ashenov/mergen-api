@extends('lora::layouts.app')

@section('content')

    <div class="card card-default">
        <div class="card-header">
            <h3>Список Кодов</h3>
        </div>

        <div class="card-body">
            <table class="table table-hover" data-form="deleteForm">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Код</th>
                    <th>Сумма от</th>
                    <th>Дата завершения</th>
                    <th>Использовано</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($models as $model)
                    <tr>
                        <td>{{$model->id}}</td>
                        <td>{{$model->title}}</td>
                        <td>{{$model->cost}}</td>
                        <td>{{$model->expired_at->format('Y-m-d')}}</td>
                        <td>{{$model->used}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('lora.promo.edit', $model->id)}}">
                                <i class="fas fa-edit" aria-hidden="true"></i></a>

                            <button class="btn btn-sm btn-danger delete" type="button"
                                    data-target="#confirm"
                                    data-toggle="modal" data-action="{{route('lora.promo.destroy', $model->id)}}">
                                <i class="fas fa-times" aria-hidden="true"></i></button>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <div class="col-md-10">
                <div class="form-group">
                    {{ $models->links() }}
                </div>
            </div>
            @include('lora::modals.base_modal')
        </div>
    </div>
@endsection

@section('js-append')

    <script>
        $('#confirm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            modal.find('#removeForm').attr('action', button.data('action'))
        })
    </script>
@endsection
