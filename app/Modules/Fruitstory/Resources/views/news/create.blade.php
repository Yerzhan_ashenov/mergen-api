@extends('lora::layouts.app')

@section('css')
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
@endsection

@section('js')
    <script src="/templateEditor/ckeditor/ckeditor.js"></script>
@endsection
@section('js-append')
    <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/plugins/datepicker/locales/bootstrap-datepicker.ru.js"></script>
    <script>
        $(document).ready(function() {
            $('#datepicker').datepicker({
                isRTL: false,
                format: 'yyyy-mm-dd',
                language: 'ru'
            });
        });

        CKEDITOR.replace('description');
        CKEDITOR.replace('short_description');
    </script>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Добавить новость</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::news.form')
        </div>
    </div>
@endsection



