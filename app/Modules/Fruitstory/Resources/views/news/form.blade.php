        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($model->exists)
            {{ Form::open(array('url' => route('lora.news.update', $model->id),'method'=> 'PUT', 'name'=>'updateNews', 'files' => true))}}
        @else
            {{ Form::open(array('url' => route('lora.news.store'),'method'=> 'POST', 'files' => true, 'name'=>'addNews'))}}
        @endif

        <div class="box-body">
            <div class="form-group">
                <label class="control-label" for="title">Заголовок</label>
                {{Form::text('title',$model->title, ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <label class="control-label" for="preview">Фото</label>
                <input type="file" id="image_id" name="image">
            </div>

            @if($model->exists and $model->image)
                <img width="150" src="{{url($model->image->getThumb())}}" alt="">
                <button type="button" class="btn btn-warning remove-image"
                        data-news_id="{{$model->id}}" data-image_id="{{$model->image->id}}"
                        data-toggle="modal" data-target="#remove-image">
                    <span  class="glyphicon glyphicon-remove"></span> удалить
                </button>
            @endif

            <div class="form-group">
                <label class="control-label" for="title">Дата</label>

                {{Form::date('created_at', date('Y-m-d',strtotime($model->created_at)),
     ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <label class="control-label" for="short_description">Краткое описание</label>
                {{Form::textarea('short_description',$model->short_description, ['class' => 'form-control',
                'id' => 'short_description', 'cols' => 10, 'rows' => 10
                ]) }}
            </div>

            <div class="form-group">
                <label class="control-label" for="description">Подробное описание</label>
                {{Form::textarea('description',$model->description, ['class' => 'form-control',
                'id' => 'description', 'cols' => 10, 'rows' => 10
                ]) }}
            </div>

            {{--<div class="form-group">--}}
                {{--<label class="control-label" for="title">На главной</label>--}}
                {{--<br>--}}
                {{--@if($model->exists)--}}
                    {{--{{Form::checkbox('on_main', null, $model->on_main, ['data-toggle' => 'toggle', 'data-on' => 'Да',--}}
                    {{--'data-off' => 'Нет']) }}--}}
                {{--@else--}}
                    {{--{{Form::checkbox('on_main',null,null,['data-toggle' => 'toggle', 'data-on' => 'Да',--}}
                    {{--'data-off' => 'Нет']) }}--}}
                {{--@endif--}}
            {{--</div>--}}

            <div class="form-group">
                <label class="control-label" for="title">Статус</label>
                {{Form::select('status_id', [1=> 'Активен', 0 => 'Не активен'],$model->status_id,['class'   => 'form-control', 'placeholder' => 'Не выбрано'])}}
            </div>

            <div class="form-group">
                @if($model->exists)
                    {{ form::submit('Обновить', ['class' => 'btn btn-primary']) }}
                @else
                    {{ form::submit('Создать', ['class' => 'btn btn-primary']) }}
                @endif

            </div>


        </div>
        {{ Form::close() }}


@include('modals.remove_image_modal')