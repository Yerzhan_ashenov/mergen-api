@extends('lora::layouts.app')

@section('content')

    <div class="card card-default">
        <div class="card-header">
            <h3>Новости</h3>
        </div>

        <div class="card-body">
            <table class="table table-hover" data-form="deleteForm">
                <thead class="thead-inverse">
                <tr>
                    <th>id</th>
                    <th>Название</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($news as $item)

                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{$item->status_id == \App\Models\News::STATUS_ACTIVE ? 'Активен' : 'Не активен'}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('lora.news.edit', $item->id)}}">
                                <i class="fas fa-edit" aria-hidden="true"></i></a>

                            <button class="btn btn-sm btn-danger delete" type="button"
                                    data-target="#confirm"
                                    data-toggle="modal" data-action="{{route('lora.news.destroy', $item->id)}}">
                                <i class="fas fa-times" aria-hidden="true"></i></button>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <div class="col-md-10">
                <div class="form-group">
                    {{ $news->links() }}
                </div>
            </div>
            @include('lora::modals.base_modal')
        </div>

    </div>

@endsection

@section('js-append')

    <script>
        $('#confirm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            modal.find('#removeForm').attr('action', button.data('action'))
        })
    </script>
@endsection