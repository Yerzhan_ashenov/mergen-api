@extends('lora::layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Редактировать новость # {{$model->id}}</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::news.form')
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
@endsection

@section('js')
    <script src="/templateEditor/ckeditor/ckeditor.js"></script>
@endsection
@section('js-append')
    <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/plugins/datepicker/locales/bootstrap-datepicker.ru.js"></script>
    <script>
        $(document).ready(function() {
            $('#datepicker').datepicker({
                isRTL: false,
                format: 'yyyy-mm-dd',
                language: 'ru'
            });
        });

        $(document).ready(function(){
            $('#remove-image').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var news_id = button.data('news_id');
                var image_id = button.data('image_id');
                var modal = $(this);
                modal.find('#removeImageForm').attr('action','/lora/news/'+ news_id +'/images/'+image_id)
            })
        });

        CKEDITOR.replace('description');
        CKEDITOR.replace('short_description');
    </script>
@endsection