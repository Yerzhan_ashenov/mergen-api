
@if($model->exists)
    {{ Form::open(array('url' => secure_url_env(route('lora.cities.update', $model->id)),'method'=> 'PUT', 'name'=>'updateCategory', 'files' => true,
     'class' => 'form-horizontal'))}}
@else
    {{ Form::open(array('url' => secure_url_env(route('lora.cities.store')),'method'=> 'POST', 'name'=>'postCategory', 'files' => true, 'class' => 'form-horizontal'))}}
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <label class="col-md-2 control-label" for="title">Страна</label>
    <div class="col-md-9">
        {{Form::select('country_id', $model->countries, $model->country_id,[
    'class'   => 'form-control',
'placeholder' => 'Страна'])}}
    </div>
</div>

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Название</label>

    <div class="col-md-9">

        {{Form::text('title', $model->title,
        ['class' => 'form-control']) }}

        @if ($errors->has('title'))
            <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
        @endif
    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label" for="title">Статус</label>
    <div class="col-md-9">
        {{Form::select('status_id', $model->statuses, $model->status_id,['class'   => 'form-control'])}}
    </div>
</div>



<div class="form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">
            @if($model->exists)Обновить@else Создать @endif
        </button>
    </div>
</div>
{{Form::close()}}
