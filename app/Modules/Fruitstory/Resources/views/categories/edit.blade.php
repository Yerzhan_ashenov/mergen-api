@extends('lora::layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Редактировать категорию</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::categories.form')
        </div>
    </div>

@endsection