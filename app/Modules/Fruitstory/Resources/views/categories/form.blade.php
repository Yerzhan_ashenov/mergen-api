
@if($model->exists)
    {{ Form::open(array('url' => secure_url_env(route('lora.categories.update', $model->id)),'method'=> 'PUT', 'name'=>'updateCategory', 'files' => true,
     'class' => 'form-horizontal'))}}
@else
    {{ Form::open(array('url' => secure_url_env(route('lora.categories.store')),'method'=> 'POST', 'name'=>'postCategory', 'files' => true, 'class' => 'form-horizontal'))}}
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Название</label>

    <div class="col-md-9">

        {{Form::text('title', $model->title, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
    <label for="icon_id" class="col-md-2 control-label">Иконка</label>
    <div class="col-md-9">
        {{Form::file('icon', ['class' => 'form-control', 'id' => 'icon']) }}

        @if ($errors->has('icon'))
            <span class="help-block">
                <strong>{{ $errors->first('icon') }}</strong>
            </span>
        @endif

        @if($model->image)
            <div class="form-group mt-5">
                <img class="img-thumbnail " src="/{{$model->image->getThumb()}}" width="150" height="150" alt="">
            </div>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
    <label for="parent_id" class="col-md-2 control-label">Родитель</label>

    <div class="col-md-9">
        {{Form::select('parent_id',[0 => 'Не выбрано'] + \App\Models\Category::parentsList($model),$model->parent_id,
        ['class' => 'form-control']) }}

        @if ($errors->has('parent_id'))
            <span class="help-block">
                <strong>{{ $errors->first('parent_id') }}</strong>
            </span>
        @endif
    </div>
</div>



<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
    <label for="position" class="col-md-2 control-label">Позиция</label>

    <div class="col-md-9">

        {{Form::number('position', $model->exists ? $model->position : \App\Models\Category::maxPosition(),
        ['class' => 'form-control']) }}

        @if ($errors->has('position'))
            <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="title">Статус</label>
    <div class="col-md-9">
    {{Form::select('status_id', [1=> 'Активен', 0 => 'Не активен'], $model->status_id,['class'   => 'form-control'])}}
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">
            @if($model->exists)Обновить@else Создать @endif
        </button>
    </div>
</div>
{{Form::close()}}
