@extends('resources.views.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Компания {{$place->name}}</div>

                <div class="panel-body">
                    <table class="table">
                    <thead>
                         <tr>
                           <th>id</th>
                           <th>Название заведения</th>
                           <th>Банковские данные</th>
                           <th>Действия</th>
                         </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>{{$place->id}}</td>
                            <td>{{$place->name}}</td>
                            <td>

                                @if($place->bank_account)
                                    <a href="/places/{{$place->id}}/bank-accounts/{{$place->bank_account->id}}/edit">Редактировать</a>
                                @else
                                <a href="/places/{{$place->id}}/bank-accounts/create">Создать</a>
                                @endif
                            </td>
                            <td>

                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
