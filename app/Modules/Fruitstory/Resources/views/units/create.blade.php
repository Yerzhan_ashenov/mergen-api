@extends('lora::layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Создать новую ед измерения</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::units.form')
        </div>
    </div>

@endsection