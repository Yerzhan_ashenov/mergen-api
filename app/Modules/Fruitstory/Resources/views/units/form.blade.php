
@if($model->exists)
    {{ Form::open(array('url' => secure_url_env(route('lora.units.update', $model->id)),'method'=> 'PUT', 'name'=>'updateCategory', 'files' => true,
     'class' => 'form-horizontal'))}}
@else
    {{ Form::open(array('url' => secure_url_env(route('lora.units.store')),'method'=> 'POST', 'name'=>'postCategory', 'files' => true, 'class' => 'form-horizontal'))}}
@endif


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Название</label>

    <div class="col-md-9">

        {{Form::text('title', $model->title, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('starting_point') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Начальное значение</label>

    <div class="col-md-9">

        {{Form::text('starting_point', $model->starting_point, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('starting_point'))
            <span class="help-block">
                <strong>{{ $errors->first('starting_point') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('step') ? ' has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Шаг</label>

    <div class="col-md-9">

        {{Form::text('step', $model->step, ['class' => 'form-control', 'id' => 'category-name']) }}

        @if ($errors->has('step'))
            <span class="help-block">
                <strong>{{ $errors->first('step') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">
            @if($model->exists)Обновить@else Создать @endif
        </button>
    </div>
</div>
{{Form::close()}}
