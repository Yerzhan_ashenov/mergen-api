@extends('lora::layouts.app')

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <h3>Список товаров по заказу #{{$order->id}}</h3>
        </div>

        <div class="card-body">
            <table class="table table-hover" data-form="deleteForm">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Название</th>
                    <th>Кол-во</th>
                    <th>Цена за ед</th>
                    <th>Сумма</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                @foreach($models as $model)
                    <tr>
                        <td>{{$model->id}}</td>
                        <td>
                            <p>{{$model->product->title ?? 'Нет названия'}}</p>
                        </td>
                        <td>
                           <p>{{$model->quantity ?? null}}</p>
                        </td>
                        <td>{{$model->product->cost ?? null}}</td>
                        <td>{{$model->sum}}</td>
                        <td>{{$model->order->status ?? 'Не задан'}}</td>
                    </tr>

                @endforeach
                <tr>
                    <td>Итого</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{$models->getCollection()->sum('sum')}}</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-10">
        <div class="form-group">
            {{ $models->links() }}
        </div>
    </div>
    @include('lora::modals.base_modal')
@endsection

@section('js-append')

    <script>
        $('#confirm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            modal.find('#removeForm').attr('action', button.data('action'))
        })
    </script>
@endsection