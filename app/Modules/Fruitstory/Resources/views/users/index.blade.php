@extends('lora::layouts.app')
@section('content')
    <div class="card card-blue">
        <div class="card-body">

            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form" role="form" method="get">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label" for="phone">Поиск по номеру</label>
                                <input type="text" name="phone" class="form-control input-sm" id="phone"
                                       value="{{request('phone')}}">
                            </div>

                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-warning filter-col">
                                <span class="glyphicon glyphicon-record"></span> Найти
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-default">
        <div class="card-header">
            <h3>Пользователи</h3>
        </div>

        <div class="card-body">
            <table class="table table-hover" data-form="deleteForm">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Адрес</th>
                    <th>Телефон</th>
                    <th>Дата регистрации</th>
                    <th>E-mail</th>
                    <th>Действия</th>
                </tr>
                </thead>

                <tbody>
                @foreach($models as $model)
                    <tr>
                        <td>{{$model->id}}</td>
                        <td>{{$model->first_name}}</td>
                        <td>{{$model->last_name}}</td>
                        <td>{{$model->address}}</td>
                        <td>{{$model->phone}}</td>
                        <td>{{$model->created_date}}</td>
                        <td>{{$model->email}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{route('lora.users.edit', $model->id)}}">
                                <i class="fas fa-edit" aria-hidden="true"></i></a>

                            <button class="btn btn-sm btn-danger delete" type="button"
                                    data-target="#confirm"
                                    data-toggle="modal" data-action="{{route('lora.users.destroy', $model->id)}}">
                                <i class="fas fa-times" aria-hidden="true"></i></button>

                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            <div class="col-md-10">
                <div class="form-group">
                    {{ $models->links() }}
                </div>
            </div>
            @include('lora::modals.base_modal')
        </div>
    </div>
@endsection
@section('js-append')

    <script>
        $('#confirm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            modal.find('#removeForm').attr('action', button.data('action'))
        })
    </script>
@endsection
