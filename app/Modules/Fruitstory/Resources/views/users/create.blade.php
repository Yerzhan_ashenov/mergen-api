@extends('lora::layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Создать пользователя</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::users.form')
        </div>
    </div>

@endsection