    @if($model->exists)
        {{ Form::open(array('url' => secure_url_env(route('lora.users.update', $model->id)),'method'=> 'PUT', 'name'=>'updateUser', 'files' => true,
         'class' => 'form-horizontal'))}}
    @else
        {{ Form::open(array('url' => secure_url_env(route('lora.users.store')),'method'=> 'POST', 'name'=>'postUser', 'files' => true, 'class' => 'form-horizontal'))}}
    @endif

    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
        <label for="firstname" class="col-md-2 control-label">Имя</label>

        <div class="col-md-9">

            {{Form::text('first_name', $model->first_name, ['class' => 'form-control', 'id' => 'category-first_name']) }}

            @if ($errors->has('first_name'))
                <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
        <label for="lastname" class="col-md-2 control-label">Фамилия</label>

        <div class="col-md-9">

            {{Form::text('last_name', $model->last_name, ['class' => 'form-control', 'id' => 'category-last_name']) }}

            @if ($errors->has('last_name'))
                <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-2 control-label">Пароль</label>

        <div class="col-md-9">
            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        <label for="password_confirmation" class="col-md-2 control-label">Повторите пароль</label>

        <div class="col-md-9">
            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
        <label for="phone_number" class="col-md-2 control-label">Телефон</label>

        <div class="col-md-9">

            {{Form::text('phone', $model->phone, ['class' => 'form-control', 'id' => 'category-phone']) }}

            @if ($errors->has('phone'))
                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-2 control-label">E-mail</label>

        <div class="col-md-9">

            {{Form::text('email', $model->email, ['class' => 'form-control', 'id' => 'category-email']) }}

            @if ($errors->has('email'))
                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">
              @if($model->exists)Обновить@else Создать @endif
            </button>
        </div>
    </div>
</form>