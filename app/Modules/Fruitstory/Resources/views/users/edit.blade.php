@extends('lora::layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="panel-heading">Редактировать пользователя # {{$model->id}}</h3>
        </div>
        <div class="card-body">
            @include('fruitstory::users.form')
        </div>
    </div>
@endsection