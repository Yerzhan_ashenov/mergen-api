@extends('lora::layouts.app')

@section('content')

    <div class="card card-default">
        <div class="card-header">
            <h3>Настройка бонусов</h3>
        </div>

        <div class="card-body">

            {{ Form::open(array('url' => secure_url_env(route('lora.bonuses.update')),'method'=> 'PUT',
                'name'=>'bonusesUpdate', 'files' => true, 'class' => 'form-horizontal'))}}


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @foreach($models as $model)
                <div class="form-group">
                    <label class="col-md-2 control-label" for="{{$model->id}}">{{$model->name}}</label>
                    <div class="col-md-9">
                        {{Form::text("settings[$model->id]", $model->value, ['class' => 'form-control']) }}
                    </div>
                </div>
            @endforeach

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Обновить
                    </button>
                </div>
            </div>
            {{Form::close()}}

        </div>
        <div class="card-footer">

        </div>
    </div>
@endsection
