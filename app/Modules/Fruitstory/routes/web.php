<?php

Route::group([
    'prefix' => 'lora',
    'as' => 'lora.',
    'middleware' => [ 'web', 'language'],
    'namespace' => 'App\Modules\Fruitstory\Http\Controllers'], function () {

    Route::group(['middleware' => 'guardAuth:admin'], function() {
        Route::resource('orders.items', 'OrderItemsController');
        Route::resource('orders', 'OrdersController');
        Route::resource('categories','CategoryController');
        Route::post('products/{product}','ProductController@update')->name('products.update');
        Route::resource('products','ProductController')->except('update');
        Route::resource('users','UserResourceController');
        Route::resource('units', 'UnitsController');
        Route::delete('news/{news_id}/images/{image_id}','NewsController@removeImage');
        Route::resource('news', 'NewsController');
        Route::resource('promo', 'PromoCodeController');
        Route::resource('delivery-areas', 'DeliveryAreasController');
        Route::resource('cities', 'CitiesController');
        Route::get('bonuses', 'BonusesController@index');
        Route::put('bonuses', 'BonusesController@update')->name('bonuses.update');

    });
});

