<?php
namespace App\Modules\Fruitstory\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Dosarkz\Lora\Installation\Modules\Lora\Models\MenuItem;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Role;
use Dosarkz\Lora\Installation\Modules\Lora\Models\MenuRole;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Module;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Menu;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if($menu = Menu::where('alias', lcfirst('Fruitstory'))->first())
        {
            $menu->menuItems()->delete();
            $menu->menuRoles()->delete();
            $menu->delete();
        }

        $module =   Module::firstOrCreate([
                    'name_ru' =>  'Fruitstory',
                    'name_en' => 'Fruitstory',
                    'menu_active' => true,
                    'description_ru' => 'Fruitstory',
                    'description_en' => 'Fruitstory',
                    'version' =>  0.01,
                    'status_id' => Module::STATUS_NEW,
                    'alias' => lcfirst('Fruitstory'),
                ]);

        $menu =   Menu::create([
            'name_ru' => 'Fruitstory',
            'name_en' =>  'Fruitstory',
            'alias' =>  lcfirst('Fruitstory'),
            'type_id' => Menu::TYPE_LEFT_SIDE_MENU,
            'module_id' => $module->id,
            'status_id' => 1,
            'position'  => 2,
        ]);

        $MenuItem =  MenuItem::create([
            'title_en'  =>  'Fruitstory',
            'title_ru' => 'Fruitstory',
            'url' => '/admin/fruitstory',
            'icon' => 'fa-briefcase',
            'position' => 1,
            'menu_id' => $menu->id,
            'status_id' => 1
        ]);

        MenuItem::create([
            'title_ru' => 'Добавить',
            'title_en'  =>  'Create',
            'url' => '/admin/fruitstory/create',
            'icon' => 'fa-plus-circle',
            'menu_id' => $menu->id,
            'parent_id' => $MenuItem->id,
            'position' => 1,
            'status_id' => 1
        ]);

        MenuItem::create([
            'title_ru' => 'Список',
            'title_en'  =>  'List',
            'url' => '/admin/fruitstory',
            'icon' => 'fa-list-ul',
            'menu_id' => $menu->id,
            'parent_id' => $MenuItem->id,
            'position' => 1,
            'status_id' => 1
        ]);

        MenuRole::create([
            'role_id' => Role::where('alias', 'admin')->first()->id,
            'menu_id'   => $menu->id,
        ]);

        MenuRole::create([
            'role_id' => Role::where('alias', 'manager')->first()->id,
            'menu_id'   => $menu->id,
        ]);

    }
}
