<?php

namespace App\Modules\Fruitstory\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\StoreUnitRequest;
use App\Models\Unit;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class UnitsController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new Unit());
        $this->setViewPath('fruitstory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = $this->getModel()->orderBy('id', 'DESC')->paginate(15);
        return $this->view('units.index', ['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->getModel();
        return $this->view('units.create', compact('model'));
    }

    /**
     * @param StoreCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUnitRequest $request)
    {
        $this->getModel()->create($request->all());
        return redirect(route('lora.units.index'))->with('success', 'Ед измерения успешно создана');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->getModel()->find($id);
        if (!$model) {
            $model = new Unit();
            return $this->view('units.create', compact('model'));
        }
        return $this->view('units.edit', ['model' => $model]);
    }

    /**
     * @param StoreUnitRequest $request
     * @param $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreUnitRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);
        $data = $request->all();
        $model->update($data);

        return redirect(route('lora.units.index'))->with('success', 'Ед. измерения изменена');
    }

    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->delete();

        return redirect(route('lora.units.index'))->with('success', 'Ед. измерения удалена');
    }

}
