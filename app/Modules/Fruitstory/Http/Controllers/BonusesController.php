<?php


namespace App\Modules\Fruitstory\Http\Controllers;


use App\Models\Setting;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BonusesController extends BasicController
{
    public function __construct()
    {
        $this->setViewPath('fruitstory');
    }

    public function index()
    {
        $models = Setting::all();
        return $this->view('bonuses.index', compact('models'));
    }

    public function update(Request $request)
    {
        if($request->has('settings'))
        {
            foreach ($request->input('settings') as $key => $item) {
                $s = Setting::whereId($key)->first();
                if(!$s) continue;
                $s->update(['value' => $item]);
            }
        }

        return redirect()->back()->with('success', 'Настройки успешно обновлены');
    }
}