<?php

namespace App\Modules\Fruitstory\Http\Controllers;

use App\Http\Requests\Request;
use App\Http\Requests\StoreProductRequest;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;
use App\Models\Product;
use App\Models\Tag;
use Dosarkz\LaravelUploader\ImageUploader;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class ProductController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new Product());
        $this->setViewPath('fruitstory');
    }

    public function index()
    {
        $models = $this->getModel()->filter()->orderBy('id', 'DESC')->paginate(15);
        $categories = $this->getModel()->ListCategories;
        return $this->view('products.index', compact('models','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->getModel();
        return $this->view('products.create', compact('model'));
    }

    /**
     * @param StoreProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->all();

        $data['alias'] = self::trans_url($request->input('title'));
        $data['user_id'] = auth('admin')->user()->id;

      $model =  $this->getModel()->create($data);

        if ($request->has('tags')) {
            $list = explode(',', $request->input('tags'));

            foreach ($list as $item) {
                Tag::create([
                    'title' => $item,
                    'product_id' => $model->id
                ]);
            }
        }

        if ($request->hasFile('image_id')) {
            $uploader = new ImageUploader($request->file('image_id'), 'images/products/');

            $model->image()->create([
                'name' => $uploader->getFileName(),
                'thumb' => $uploader->getThumb(),
                'path' => $uploader->destination
            ]);
        }

        return redirect(route('lora.products.index'))->with('success', 'success');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        return $this->view('products.edit', compact('model'));
    }

    /**
     * @param StoreProductRequest $request
     * @param $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreProductRequest $request, $id)
    {
        $model = $this->getModel()->find($id);
        $data = $request->all();

        if ($request->has('tags')) {
            $list = explode(',', $request->input('tags'));

            $model->tags()->delete();

            foreach ($list as $item) {
                Tag::firstOrCreate([
                    'title' => $item,
                    'product_id' => $model->id
                ]);
            }
        }

        if ($request->hasFile('image_id')) {
            $model->images()->delete();
            $uploader = new ImageUploader($request->file('image_id'), 'images/products/');

            $model->image()->create([
                'name' => $uploader->getFileName(),
                'thumb' => $uploader->getThumb(),
                'path' => $uploader->destination
            ]);
        }
        $model->update($data);
        return redirect(route('lora.products.index'))->with('success', 'success');
    }

    /**
     * @param $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = $this->getModel()->find($id);
        $model->delete();

        return redirect(route('lora.products.index'))->with('success', 'deleted');
    }

}
