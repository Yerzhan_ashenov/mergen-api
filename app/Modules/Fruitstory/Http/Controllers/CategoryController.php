<?php

namespace App\Modules\Fruitstory\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Models\Category;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;
use Dosarkz\LaravelUploader\ImageUploader;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;
use Illuminate\Http\Request;

class CategoryController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new Category());
        $this->setViewPath('fruitstory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = $this->getModel()->filter()->orderBy('updated_at', 'DESC')->paginate(15);
        return $this->view('categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->getModel();
        return $this->view('categories.create', compact('model'));
    }

    /**
     * @param StoreCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCategoryRequest $request)
    {
        $request->merge([
            'alias' => self::trans_url($request->input('title'))
        ]);

        $data = $request->all();
        $model = Category::create($data);
        $this->uploadImage($model, $request);

        return redirect('lora/categories')->with('success', 'success');
    }

    private function uploadImage($model, $request)
    {
        if ($request->hasFile('icon')) {
            $model->image()->delete();
            $uploader = new ImageUploader($request->file('icon'), 'images/icons/');

            $model->image()->create([
                'name' => $uploader->getFileName(),
                'thumb' => $uploader->getThumb(),
                'path' => $uploader->destination
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $model = $this->getModel()->find($id);

        if (!$model) {
            $model = new Category();
            return $this->view('categories.create', compact('model'));
        }

        return $this->view('categories.edit', ['model' => $model]);
    }


    public function update(StoreCategoryRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);
        $data = $request->all();
        $model->update($data);
        $this->uploadImage($model, $request);

        return redirect('lora/categories')->with('success', 'success');
    }


    public function destroy($id)
    {
        $category = $this->getModel()->findOrFail($id);
        $category->delete();
        return redirect('lora/categories')->with('success', 'deleted');
    }

}
