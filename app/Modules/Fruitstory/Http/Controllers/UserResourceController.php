<?php

namespace App\Modules\Fruitstory\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Requests\StoreUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;
use Illuminate\Http\Request;

class UserResourceController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new User());
        $this->setViewPath('fruitstory');
    }

    public function index(Request $request)
    {
        $models = $this->getModel()->filter()->orderBy('id', 'DESC')->paginate(15);
        return $this->view('users.index', compact('models'));
    }

    public function store(StoreUsersRequest $request)
    {
        $request->merge([
            'password' => bcrypt($request->input('password')),
            'api_token' => str_random(60),
            'phone' => purify_phone_number($request->input('phone'))
        ]);

        $model = $this->getModel()->create($request->all());

        $user_role = UserRole::firstOrCreate([
            'user_id' => $model->id,
            'role_id' => Role::ROLE_USER
        ]);
        $model->user_role_id = $user_role->id;
        $model->save();
        return redirect(route('lora.users.index'))->with('success', 'Пользователь успешно создан');
    }

    public function create()
    {
        $model = $this->getModel();
        return $this->view('users.create', compact('model'));
    }

    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        return $this->view('users.edit', compact('model'));
    }

    public function update(UpdateUsersRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);

        if($request->has('password') && !empty($request->input('password'))){
            $request->merge([
                'password' => bcrypt($request->input('password')),
            ]);
        }

        if($request->has('phone') && !empty($request->input('phone'))){
            $request->merge([
                'phone' => purify_phone_number($request->input('phone'))
            ]);
        }

        $model->update($request->validated());
        return redirect()->back()->with('success', 'Пользователь успешно обновлен');
    }

    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->userRole->delete();
        $model->delete();
        return redirect()->back()->with('success', 'Пользователь успешно удален');
    }
}
