<?php


namespace App\Modules\Fruitstory\Http\Controllers;


use App\Http\Requests\StoreCityRequest;
use App\Http\Requests\UpdateCityRequest;
use App\Models\City;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class CitiesController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new City());
        $this->setViewPath('fruitstory');
    }

    public function index()
    {
        $models = $this->getModel()->orderBy('created_at','desc')->paginate(10);
        return $this->view('cities.index', compact('models'));
    }

    public function create()
    {
        $model = $this->getModel();
        return $this->view('cities.create', compact('model'));
    }

    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        return $this->view('cities.edit', compact('model'));
    }

    public function store(StoreCityRequest $request)
    {
        $this->getModel()->create($request->all());
        return redirect(route('lora.cities.index'))->with('success', 'Город успешно создан');
    }

    public function update(UpdateCityRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->update($request->all());
        return redirect(route('lora.cities.index'))->with('success', 'Город успешно обновлен');
    }

    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->delete();
        return redirect(route('lora.cities.index'))->with('success', 'Город успешно удален');
    }
}