<?php

namespace App\Modules\Fruitstory\Http\Controllers;

use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;
use Dosarkz\Lora\Installation\Modules\Lora\Models\Image;
use App\Models\News;
use App\Repositories\File\NewsImageUploader;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Dosarkz\LaravelUploader\ImageUploader;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class NewsController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new News());
        $this->setViewPath('fruitstory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = $this->getModel()->orderBy('created_at','DESC')->paginate();
        return $this->view('news.index', ['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->getModel();
        $model->created_at = Carbon::now();
        return $this->view('news.create', ['model' => $model]);
    }

    /**
     * @param Requests\StoreNewsRequest $request
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Requests\StoreNewsRequest $request)
    {
        $request->merge([
            'user_id' => auth('admin')->user()->id,
            'alias' => $this->trans_url($request->input('title')),
            'created_at'  => date('Y-m-d h:i:s',strtotime($request->input('created_at'))),
        ]);

        $news = $this->getModel()->create($request->all());

        if ($request->input('on_main') == 'on')
        {
            $news->update([
                'on_main' => true
            ]);
        }

        $this->uploadImage($news, $request);
        return redirect(route('lora.news.index'))->with('success','Успешно');
    }

    private function uploadImage($model, $request) : bool
    {
        if ($request->hasFile('image'))
        {
            $uploader = new ImageUploader($request->file('image'), 'images/news/');

            $model->image()->create([
                'name' => $uploader->getFileName(),
                'thumb' => $uploader->getThumb(),
                'path' => $uploader->destination
            ]);

            return true;
        }
        return false;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        return $this->view('news.edit', ['model' => $model]);
    }

    public function show($id)
    {
        $model = $this->getModel()->findOrFail($id);
        return view('news.show',compact('model'));
    }

    /**
     * @param Requests\StoreNewsRequest $request
     * @param $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Requests\StoreNewsRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);

        $request->merge([
            'created_at' => date('Y-m-d h:i:s',strtotime($request->input('created_at')))
        ]);

        if (!$request->has('on_main'))
        {
            $request->merge([
                'on_main' => 0
            ]);
        }else{
            if ($request->input('on_main') == 'on')
            {
                $request->merge([
                    'on_main' => 1
                ]);
            }
        }

        $model->update($request->all());
        $this->uploadImage($model, $request);
        return redirect(route('lora.news.index'))->with('success','Успешно');
    }

    /**
     * @param $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $news = $this->getModel()->findOrFail($id);
        $news->delete();
        return redirect(route('lora.news.index'))->with('success', 'Новость успешно удалена!');
    }

    public function removeImage($news_id, $image_id)
    {
        $image = Image::findOrFail($image_id);

        if(file_exists(public_path($image->getThumb())))
        {
            unlink(public_path($image->getThumb()));
        }
        if(file_exists(public_path($image->getFullImage())))
        {
            unlink(public_path($image->getFullImage()));
        }

        $image->delete();
        return redirect()->back()->with('success', 'Фото удалено');
    }

}
