<?php


namespace App\Modules\Fruitstory\Http\Controllers;


use App\Models\Order;
use App\Models\OrderItem;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class OrderItemsController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new OrderItem());
        $this->setViewPath('fruitstory');
    }

    public function index(Order $order)
    {
        $models = $order->orderItems()->orderBy('updated_at', 'DESC')->paginate(15);
        return $this->view('order_items.index', compact('models', 'order'));
    }
}