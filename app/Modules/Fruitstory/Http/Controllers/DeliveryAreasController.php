<?php
/**
 * Created by PhpStorm.
 * User: dosarkz
 * Date: 2019-03-23
 * Time: 17:04
 */

namespace App\Modules\Fruitstory\Http\Controllers;


use App\Http\Requests\StoreDeliveryAreaRequest;
use App\Http\Requests\UpdateDeliveryAreaRequest;
use App\Models\City;
use App\Models\DeliveryArea;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class DeliveryAreasController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new DeliveryArea());
        $this->setViewPath('fruitstory');
    }

    public function index()
    {
        $models = $this->getModel()->orderBy('created_at','desc')->paginate(10);
        $cities = City::active()->pluck('title', 'id');
        return $this->view('delivery_areas.index', compact('models', 'cities'));
    }

    public function create()
    {
        $model = $this->getModel();
        $cities = City::active()->pluck('title', 'id');
        return $this->view('delivery_areas.create', compact('model', 'cities'));
    }

    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $cities = City::active()->pluck('title', 'id');
        return $this->view('delivery_areas.edit', compact('model', 'cities'));
    }

    public function store(StoreDeliveryAreaRequest $request)
    {
        $this->getModel()->create($request->all());
        return redirect(route('lora.delivery-areas.index'))->with('success', 'Район успешно создан');
    }

    public function update(UpdateDeliveryAreaRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->update($request->validated());
        return redirect(route('lora.delivery-areas.index'))->with('success', 'Район успешно обновлен');
    }

    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->delete();
        return redirect(route('lora.delivery-areas.index'))->with('success', 'Район успешно удален');
    }
}