<?php
/**
 * Created by PhpStorm.
 * User: dosarkz
 * Date: 2019-03-23
 * Time: 17:33
 */

namespace App\Modules\Fruitstory\Http\Controllers;

use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class OrdersController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new Order());
        $this->setViewPath('fruitstory');
    }

    public function index()
    {
        $models = $this->getModel()::orderBy('created_at', 'desc')->paginate('10');
        return $this->view('orders.index', compact('models'));
    }

    /**
     * Remove the order
     *
     * */
    public function destroy($id)
    {
        $model = Order::findOrFail($id);
        $model->orderItems()->delete();
        $model->delete();

        return redirect()->back()->with('success', 'Success');
    }

    public function edit($id)
    {
        $model = Order::findOrFail($id);
        return $this->view('orders.edit', compact('model'));
    }

    public function update(UpdateOrderRequest $request, $id)
    {
        $model = Order::findOrFail($id);
        $model->update($request->all());

        return redirect(route('lora.orders.index'))->with('success', 'Success');
    }
}