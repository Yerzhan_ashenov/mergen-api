<?php
/**
 * Created by PhpStorm.
 * User: dosarkz
 * Date: 2019-03-23
 * Time: 17:04
 */

namespace App\Modules\Fruitstory\Http\Controllers;


use App\Http\Requests\StorePromoCodeRequest;
use App\Http\Requests\UpdatePromoCodeRequest;
use App\Models\PromoCode;
use App\Models\User;
use Carbon\Carbon;
use Dosarkz\Lora\Installation\Modules\Lora\Http\Controllers\BasicController;

class PromoCodeController extends BasicController
{
    public function __construct()
    {
        $this->setModel(new PromoCode());
        $this->setViewPath('fruitstory');
    }
    public function index()
    {
        $models = $this->getModel()->orderBy('created_at','desc')->paginate(10);
        return $this->view('promo.index', compact('models'));
    }

    public function create()
    {
        $model = $this->getModel();
        $model->expired_at = Carbon::now()->addDay();
        $users = User::where('status_id', User::STATUS_ACTIVE)->get()->pluck('full_name', 'id');
        return $this->view('promo.create', compact('model', 'users'));
    }

    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $users = User::where('status_id', User::STATUS_ACTIVE)->get()->pluck('full_name', 'id');
        return $this->view('promo.edit', compact('model', 'users'));
    }

    public function store(StorePromoCodeRequest $request)
    {
        $request->merge([
            'status_id' => PromoCode::STATUS_FREE
        ]);

        $this->getModel()::create($request->all());
        return redirect(route('lora.promo.index'))->with('success', 'Промо успешно создан');
    }

    public function update(UpdatePromoCodeRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->update($request->all());

        return redirect(route('lora.promo.index'))->with('success', 'Промо успешно обновлен');
    }


    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->delete();
        return redirect(route('lora.promo.index'))->with('success', 'Промо успешно удален');
    }
}