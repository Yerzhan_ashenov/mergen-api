<?php
/**
 * Created by PhpStorm.
 * User: noname
 * Date: 10/25/19
 * Time: 15:07
 */

namespace App\Http\Controllers\Api;


use App\Http\Requests\GetPromoCodeRequest;
use App\Models\City;
use App\Models\DeliveryArea;
use App\Models\Order;
use App\Models\PromoCode;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Dosarkz\Paybox\Facades\Paybox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AppController extends ApiController
{
    /**
     * List Delivery areas
     * Отобразить места доставки
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeliveryAreas(City $city)
    {
        try {
            $models = DeliveryArea::where('status_id', DeliveryArea::STATUS_ACTIVE)
                ->where('city_id', $city->id)->get();
            $this->response->content = $models;

            return $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }

    }

    /**
     * Check to Promo code
     * Проверить промо код
     * @param GetPromoCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPromoCode(GetPromoCodeRequest $request)
    {
        try {
            $model = PromoCode::where('title', $request->input('code'))
                ->where('expired_at', '>', Carbon::now('Asia/Almaty'))
                ->first();

            if ($model) {
                $model->update([
                    'token' => (new UserRepository())->generateHash()
                ]);
            } else {
                $this->response->errors = [
                    'message' => 'Код не найден либо недействителен'
                ];
                return $this->sendResponse(422);
            }

            $this->response->content = $model;
            return $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }

    public function getCities()
    {
        try {
            $model = City::where('status_id', City::STATUS_ACTIVE)->get();
            $this->response->content = $model;
            return $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }


}