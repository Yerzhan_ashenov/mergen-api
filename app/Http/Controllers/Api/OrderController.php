<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ConfirmOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Models\DeliveryArea;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\PromoUse;
use App\Models\Setting;
use App\Models\UserBonus;
use Carbon\Carbon;
use Dosarkz\Paybox\Facades\Paybox;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class OrderController extends ApiController
{
    /**
     * List my own orders
     * Показать список заказов
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $models = Order::where('user_id', auth()->user()->id)
                ->orderBy('created_at', 'desc')
                ->get();

            $this->response->content = $models;
            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }

    }

    /**
     * Show a Order detail
     * Показать деталь заказа
     * @param $order_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($order_id)
    {
        try {
            $model = Order::find($order_id);
            $this->response->content = $model;

            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

    /**
     * Order create
     * Создать заказ с продуктами
     *  products[1][id], products[1][cost]
     * @param StoreOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeGuest(StoreOrderRequest $request)
    {
        try {
            $order = Order::create([
                'amount' => 0,
                'status_id' => Order::STATUS_CREATED,
                'type_payment' => $request->input('type_payment', Order::TYPE_PAYMENT_CASH),
                'comment' => $request->input('comment'),
                'delivery_type' => $request->input('delivery_type'),
                'available_sum' => $request->input('available_sum'),
                'area_id' => $request->input('area_id'),
                'city_id' => $request->input('city_id'),
                'delivery_address' => $request->input('delivery_address'),
                'delivery_date' => $request->input('delivery_date'),
                'promo_code' => $request->input('promo_code'),
                'name' => $request->input('name'),
                'phone' => purify_phone_number($request->input('phone')),
            ]);

            $amount = 0;

            foreach ($request->input('products') as $item) {
                $product = Product::find($item['id']);
                OrderItem::create([
                    'product_id' => $item['id'],
                    'sum' => $product->cost * $item['quantity'],
                    'quantity' => $item['quantity'],
                    'order_id' => $order->id,
                ]);
                $amount += $product->cost * $item['quantity'];
            }

            $promo = PromoCode::whereTitle($request->input('promo_code'))
                ->where('expired_at', '>=', Carbon::now('Asia/Almaty'))
                ->first();

            if ($promo) {
                $amount = $amount - ($amount * $promo->cost / 100);
                $promo->update([
                    'status_id' => PromoCode::STATUS_USED
                ]);
            }

            $area = DeliveryArea::find($request->input('area_id'));
            if ($area) {
                $amount = $amount + $area->cost;
            }

            $order->update(['amount' => $amount]);
            $this->response->content = $order;
            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = [
                'error' =>  $e->getMessage()
            ];
            return $this->sendResponse(400);
        }
    }

    public function store(StoreOrderRequest $request)
    {
        try {
            $order = Order::create([
                'amount' => 0,
                'user_id' => auth()->user()->id,
                'status_id' => Order::STATUS_CREATED,
                'type_payment' => $request->input('type_payment', Order::TYPE_PAYMENT_CASH),
                'comment' => $request->input('comment'),
                'delivery_type' => $request->input('delivery_type'),
                'available_sum' => $request->input('available_sum'),
                'area_id' => $request->input('area_id'),
                'city_id' => $request->input('city_id'),
                'delivery_address' => $request->input('delivery_address'),
                'delivery_date' => $request->input('delivery_date'),
                'promo_code' => $request->input('promo_code'),
                'name' => $request->input('name'),
                'phone' => purify_phone_number($request->input('phone')),
            ]);

            $amount = 0;

            foreach ($request->input('products') as $item) {
                $product = Product::find($item['id']);
                OrderItem::create([
                    'product_id' => $item['id'],
                    'sum' => $product->cost * $item['quantity'],
                    'quantity' => $item['quantity'],
                    'order_id' => $order->id,
                    'user_id' => auth()->user()->id
                ]);
                $amount += $product->cost * $item['quantity'];
            }

            $promo = PromoCode::whereTitle($request->input('promo_code'))
                ->where('expired_at', '>=', Carbon::now('Asia/Almaty'))
                ->first();
            $bonus = Setting::where('slug', 'percentage_of_bonus')->active()->first();

            if ($promo) {
                $amount = $amount - ($amount * $promo->cost / 100);
                $promo->update([
                    'status_id' => PromoCode::STATUS_USED
                ]);
                PromoUse::create([
                    'user_id' => $request->user()->id,
                    'promo_id' => $promo->id,
                    'order_id' => $order->id,
                ]);
                // add bonus to owner of promo
                if ($promo->user_id && $request->user('api')->id != $promo->user_id && $bonus) {
                    UserBonus::create([
                        'order_id' => $order->id,
                        'user_id' => $promo->user_id,
                        'amount' => $amount * $promo->percentage_of_user / 100
                    ]);
                }
            } else {
                if ($bonus) {
                    $userBonus = $request->user('api')->bonus;
                    // use bonus
                    if ($request->has('bonus') && $request->input('bonus') == true && $userBonus > 0) {
                        $amount = $amount - $userBonus;

                        $request->user('api')->userBonuses()->update([
                            'status_id' => UserBonus::STATUS_USED
                        ]);
                    } else {
                        UserBonus::create([
                            'order_id' => $order->id,
                            'user_id' => $request->user('api')->id,
                            'amount' => $amount * $bonus->value / 100
                        ]);
                    }
                }
            }

            $area = DeliveryArea::find($request->input('area_id'));
            if ($area) {
                $amount = $amount + $area->cost;
            }

            $order->update(['amount' => $amount]);
            $this->response->content = $order;
            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

    /**
     * Show list orders from admin
     * Показать список заказов для авмина
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminIndex()
    {
        try {
            $models = Order::orderBy('created_at', 'desc')->get();

            $this->response->content = $models;
            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }


    public function edit()
    {

    }

    /**
     * Moderate order from admin
     * Модерация заказов
     * @param $id
     * @param ConfirmOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function moderateOrder($id, ConfirmOrderRequest $request)
    {
        try {
            $model = Order::findOrFail($id);

            switch ($request->input('status')) {
                case 'complete':
                    $model->status_id = Order::STATUS_COMPLETED;
                    break;
                case 'confirm':
                    $model->status_id = Order::STATUS_CONFIRMED;
                    break;
                case 'reject':
                    $model->status_id = Order::STATUS_REJECTED;
                    break;
            }

            $model->save();

            $this->response->content = $model;
            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }

    /**
     * Remove the order
     * Удалить заказ
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $model = Order::findOrFail($id);
            $model->orderItems()->delete();
            $model->delete();

            $this->response->content = $model;
            return $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }

    public function pay(Order $order)
    {
        try {
            $payStatus = Paybox::generate([
                'order' => "order-" . $order->id,
                'amount' => $order->amount,
                'currency' => 'KZT',
                'description' => 'Оплата по заказу №' . $order->id,
                'expires_at' => Carbon::tomorrow('Asia/Almaty')->toDateTimeString(),
                'x_idempotency_key' => $order->uuid,
                "options" => [
                    "callbacks" => [
                        "result_url" => "http://fruitstory.kz/payment-status",
                        "success_url" => "http://fruitstory.kz/payment-status",
                        "back_url" => "http://fruitstory.kz/payment-status",
                    ]
                ]
            ]);

            if ($payStatus instanceof MessageBag) {
                $this->response->content = $payStatus;
                return $this->sendResponse(422);
            }

            $order->update([
                'bank_order_id' => $payStatus['response']['id']
            ]);

            $this->response->content = [
                'url' => $payStatus['response']['payment_page_url']
            ];
            return $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }

    public function payStatus(Order $order)
    {
        try {
            if (!$order->bank_order_id) {
                throw new \ErrorException('bank order id not found');
            }

            $payStatus = Paybox::paymentInfo($order->bank_order_id);
            if (array_key_exists('status', $payStatus['response'])) {
                Log::info($payStatus['response']);

                if ($payStatus['response']['status']['code'] === 'success') {
                    $order->update([
                        'is_paid' => true
                    ]);
                }
            }
            $this->response->content = [
                'url' => $payStatus['response']
            ];
            return $this->sendResponse();

        } catch (\Exception $e) {
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }
    }


}