<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends ApiController
{
    /**
     * Show an user resource
     * Показать объект пользователя
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            $this->response->content = $user;
            return $this->sendResponse();
        }catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

    public function profile(Request $request)
    {
        try {
            $this->response->content = $request->user('api');
            return $this->sendResponse();
        }catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

    /**
     * Update an user resource
     * Обновить данные пользователя
     * @param UpdateUserRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());

            if($request->has('password'))
            {
                $user->update([
                    'password' => bcrypt($request->input('password'))
                ]);
            }

            $this->response->content = $user;
            return $this->sendResponse();
        }catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

    /**
     * Destroy an user resource
     * Удалить пользователя
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();

            $this->response->content = $user;
            return $this->sendResponse();
        }catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

}
