<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ResendSmsRequest;
use App\Http\Requests\SmsConfirmationRequest;
use App\Models\Role;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\UserRole;
use App\Models\UserSms;
use App\Notifications\SendSms;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{
    /**
     * Register an User
     * Регистрация пользователя
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        try {
            DB::beginTransaction();
            $phone = purify_phone_number($request->input('phone'));
            $userSms = UserSms::where('phone', $phone)->first();
            $code = rand(111111, 999999);
            if ($userSms) {
                $userSms->update([
                    'code' => $code,
                    'status_id' => UserSms::STATUS_CREATED
                ]);
            } else {
                UserSms::create([
                    'phone' => $phone,
                    'code' => $code
                ]);
            }

            $user = User::where('phone', $phone)->first();

            if (!$user) {
                $user = User::create([
                    'phone' => $phone,
                    'password' => Hash::make(str_random(8)),
                    'api_token' => Hash::make(str_random(60)),
                ]);

                $user_role = UserRole::firstOrCreate([
                    'user_id' => $user->id,
                    'role_id' => Role::ROLE_USER
                ]);

                $user->update([
                    'user_role_id' => $user_role->id,
                    'status_id' => User::STATUS_DISABLE
                ]);

                if ($request->has('platform') && $request->has('version') &&
                    $request->has('model') && $request->has('uuid') && $request->has('push_token')) {
                    $user_device = UserDevice::firstOrCreate([
                        'uuid' => $request->input('uuid'),
                        'user_id' => $user->id,
                    ]);

                    $user_device->update([
                        'model' => $request->input('model'),
                        'platform' => $request->input('platform'),
                        'version' => $request->input('version'),
                        'push_token' => $request->input('push_token'),
                    ]);
                }

            }

            $user->notify(new SendSms($code));
            DB::commit();
            $this->response->messages = ['Смс успешно отправлено'];

            return $this->sendResponse();

        } catch (Exception $e) {
            DB::rollBack();
            $this->response->errors = [
                'error' => $e->getMessage()
            ];
            return $this->sendResponse(400);
        }
    }

    public function confirmSms(SmsConfirmationRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $phone = purify_phone_number($request->input('phone'));
            $user = User::where('phone', $phone)->first();
            if (!$user) {
                $this->response->messages = [
                    'message' => 'User not found'
                ];
                return $this->sendResponse(400);
            }

            if (!$user->userSms) {
                $this->response->messages = [
                    'message' => 'Смс код введен некорректно'
                ];
                return $this->sendResponse(400);
            }

            if ($user->userSms->code == $request->input('code')) {
                $user->userSms->update([
                    'status_id' => UserSms::STATUS_USED
                ]);
                $user->update([
                    'status_id' => User::STATUS_ACTIVE,
                ]);
                auth()->login($user);
                $this->response->content = $user;
                $this->response->messages = ['Смс подтверждено'];
                DB::commit();
            }

            return $this->sendResponse();
        } catch (Exception $e) {
            DB::rollBack();
            $this->response->errors = [
                'error' => $e->getMessage()
            ];
            return $this->sendResponse(400);
        }
    }

    public function resendSms(ResendSmsRequest $request)
    {
        try {
            DB::beginTransaction();
            $phone = purify_phone_number($request->input('phone'));
            $user = User::where('phone', $phone)->first();
            $code = rand(111111, 999999);
            $userSms = UserSms::where('phone', $phone)->first();

            if (!$user || !$userSms) {
                $this->response->messages = [
                    'message' => 'User not found'
                ];
                return $this->sendResponse(400);
            }

            if($userSms->tries > 3)
            {
                $this->response->messages = [
                    'message' => 'Количество попыток отправки смс ограничено. Свяжитесь со службой поддержки'
                ];
                return $this->sendResponse(400);
            }

            $userSms->update([
                'code' => $code,
                'status_id' => UserSms::STATUS_CREATED,
                'tries' => $userSms->tries + 1
            ]);

            $user->notify(new SendSms($code));
            DB::commit();
            $this->response->messages = ['Смс успешно отправлено'];

            return $this->sendResponse();
        } catch (Exception $e) {
            DB::rollBack();
            $this->response->errors = [
                'error' => $e->getMessage()
            ];
            return $this->sendResponse(400);
        }
    }


}
