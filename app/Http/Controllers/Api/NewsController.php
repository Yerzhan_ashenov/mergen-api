<?php
namespace App\Http\Controllers\Api;

use App\Models\News;

class NewsController extends ApiController
{
    /**
     * List News
     * Отобразить все новости
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $news = News::where('status_id',News::STATUS_ACTIVE)
                ->with('image')
                ->orderBy('created_at')->paginate();

            $this->response->content = $news;
            return $this->sendResponse();

        }
        catch (\Exception $e){
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }

    }

    /**
     * Show a News item
     * Показать новость
     * @param int $id   News_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $News = News::with('image')->findOrFail($id);
            $this->response->content = $News;

            return $this->sendResponse();

        }
        catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

}