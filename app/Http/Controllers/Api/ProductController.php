<?php
namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\Product;

class ProductController extends ApiController
{
    /**
     * List Products
     * Показать список товаров
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($category_id)
    {
        try {
            $category = Category::findOrFail($category_id);

            $models = Product::where('category_id',$category->id)
                ->where('status_id',Product::STATUS_ACTIVE)
                ->orWhere(function($query) use($category){
                    $query->where('status_id',Product::STATUS_ACTIVE)
                        ->whereIn('category_id', $category->subs->pluck('id')->toArray());
                })
                ->orderBy('created_at', 'desc')
                ->with('image')
                ->paginate(20);

            $this->response->content = $models;
            return $this->sendResponse();

        }
        catch (\Exception $e){
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }

    }

    /**
     * Show a Product item by id and category
     * Показать деталь продукта
     * @param $category_id
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($category_id, $product_id)
    {
        try {
            $model = Product::with('image')->findOrFail($product_id);
            $this->response->content = $model;

            return $this->sendResponse();

        }
        catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }
}