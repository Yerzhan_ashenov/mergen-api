<?php
namespace App\Http\Controllers\Api;

use App\Models\Category;

class CategoryController extends ApiController
{
    /**
     * List Categories
     * Отобразить все категории
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $categories = Category::where('status_id',Category::STATUS_ACTIVE)
                ->where('parent_id',0)
                ->orderBy('position')
                ->with('image')
                ->get()->makeHidden('subs');

            $this->response->content = $categories;

            return $this->sendResponse();

        }
        catch (\Exception $e){
            $this->response->errors = trans('messages.critical_error');
            return $this->sendResponse(400);
        }

    }

    /**
     * Show a category
     * Показать категорию
     * @param int $id   category_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $category = Category::with('image')->findOrFail($id);
            $this->response->content = $category;

            return $this->sendResponse();

        }
        catch (\Exception $e){
            $this->response->errors = $e->getMessage();
            return $this->sendResponse(400);
        }
    }

}