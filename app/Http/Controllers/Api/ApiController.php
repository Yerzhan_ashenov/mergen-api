<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\FileRepository;
use App\Repositories\ResponseRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
abstract class ApiController extends Controller
{
    /**
     * @var ResponseRepository
     */
    protected $response;
    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var FileRepository
     */
    protected $fileRepository;
    /**
     * @var
     */
    protected $notification;
    /**
     * @var
     */
    protected $smsRepository;

    /**
     * @param ResponseRepository $response
     * @param UserRepository $userRepository
     * @param FileRepository $fileRepository
     */
    public function __construct(ResponseRepository $response, UserRepository $userRepository,
                                FileRepository $fileRepository)
    {
        $this->response = $response;
        $this->userRepository = $userRepository;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        switch ($name)
        {
            case 'user':
                return Auth::guard('api')->user();
                break;
            case 'device':
                return Auth::guard('api')->user()->device;
                break;
            default:
                return null;
                break;
        }
    }


    /**
     * @param int $status_code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($status_code = 200)
    {
        return  $this->response->get($status_code, $this->response->content, $this->response->errors,
                $this->response->messages);
    }
}