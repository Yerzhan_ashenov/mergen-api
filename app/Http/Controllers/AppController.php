<?php


namespace App\Http\Controllers;


use App\Models\Order;
use Dosarkz\Paybox\Facades\Paybox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Exception\NotFoundException;

class AppController extends Controller
{
    public function paymentStatus(Request $request)
    {
        if (!$request->has('pg_payment_id') || is_null($request->input('pg_payment_id'))) {
            abort(403);
        }

        $payStatus = Paybox::paymentInfo($request->input('pg_payment_id'));
        if (array_key_exists('status', $payStatus['response'])) {
            Log::info($payStatus['response']);
            if ($payStatus['response']['status']['code'] === 'success') {
                $order = Order::where('bank_order_id', $request->input('pg_payment_id'))->first();
                if ($order) {
                    $order->update([
                        'is_paid' => true
                    ]);
                }
            }
        }
        return view('payment_status', compact('payStatus'));
    }

}