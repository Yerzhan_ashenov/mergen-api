<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


abstract class CrudController extends Controller
{
    /**
     * @var string path to view folder
     */
    protected $viewPath = '';

    /**
     * @var string model resource url, example 'categories'
     */
    protected $modelUrl = '';

    /**
     * @var string default = $modelUrl
     */
    protected $afterSaveRedirectUrl = '';

    /**
     * @var string full class name, example '\App\Models\Category'
     */
    protected $modelClass = '';

    /**
     * @var array rules for Validator to store modelClass object
     */
    protected $storeValidationRules = [];

    /**
     * @var array rules for Validator to update modelClass object
     */
    protected $updateValidationRules = [];

    /**
     * @var array list of disabled actions
     */
    protected $disabledActions = [];


    protected $orderBy = null;

    public function __construct()
    {
        if($this->afterSaveRedirectUrl == '') {
            $this->afterSaveRedirectUrl = $this->modelUrl;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->checkForDisabled(__FUNCTION__);
        $model = new $this->modelClass();

        $orderBy = $this->orderBy;
        $models = $model->when($this->orderBy, function ($query) use ($orderBy) {
            foreach($orderBy as $col => $type) {
                $query->orderBy($col, $type);
            }
            return $query;
        }, function ($query) {
            return $query->orderBy('id', 'DESC');
        })->paginate(15);

        return view(sprintf('%s.index', $this->viewPath), ['models' => $models, 'url' => $this->modelUrl]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkForDisabled(__FUNCTION__);
        $model = new $this->modelClass();
        return view(sprintf('%s.create', $this->viewPath), ['model' => $model, 'url' => $this->modelUrl, 'viewPath' => $this->viewPath]);
    }

    /**
     * @description invoked before saving new model
     * @param $data Request data
     */
    protected function beforeSave(&$data, $request = null)
    {    }

    protected function afterSave($model)
    {    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->checkForDisabled(__FUNCTION__);
        $this->validate($request, $this->storeValidationRules);

        $data = $request->all();
        $this->beforeSave($data, $request);
        $model = (new $this->modelClass())->create($data);
        $this->afterSave($model);

        return redirect($this->afterSaveRedirectUrl)->with('success','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkForDisabled(__FUNCTION__);
        $model = (new $this->modelClass())->find($id);

        return view(sprintf('%s.show', $this->viewPath), ['model' => $model, 'url' => $this->modelUrl, 'viewPath' => $this->viewPath]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkForDisabled(__FUNCTION__);
        $model = (new $this->modelClass())->find($id);
        if (!$model) {
            return $this->create();
        }
        return view(sprintf('%s.edit', $this->viewPath), ['model' => $model, 'url' => $this->modelUrl, 'viewPath' => $this->viewPath]);
    }

    /**
     * @description invoked before updating new model
     * @param $data Request data
     */
    protected function beforeUpdate(&$data, $request = null)
    {
        $this->beforeSave($data, $request);
    }

    protected function afterUpdate($model)
    {
        $this->afterSave($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkForDisabled(__FUNCTION__);
        $this->validate($request, $this->updateValidationRules);

        $model = (new $this->modelClass())->find($id);

        $data = $request->all();
        $this->beforeUpdate($data, $request);
        $model->update($data);
        $this->afterUpdate($model);

        return redirect($this->modelUrl)->with('success','success');
    }

    protected function beforeDestroy($id)
    {   }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->checkForDisabled(__FUNCTION__);
        $model = (new $this->modelClass())->find($id);
        $this->beforeDestroy($id);
        $model->delete();

        return redirect($this->modelUrl)->with('success', 'deleted');
    }

    public function delete($id)
    {
        $this->checkForDisabled(__FUNCTION__);
        $model = (new $this->modelClass())->find($id);
        return view(sprintf('%s.delete', $this->viewPath), ['model' => $model, 'url' => $this->modelUrl, 'viewPath' => $this->viewPath]);
    }

    private function checkForDisabled($functionName)
    {
        if(in_array($functionName, $this->disabledActions)) {
            abort(404);
        }
    }

}
