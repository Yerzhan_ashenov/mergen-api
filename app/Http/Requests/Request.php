<?php

namespace App\Http\Requests;

use App\Repositories\ResponseRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

abstract class Request extends FormRequest
{

    protected function failedAuthorization()
    {
        return new JsonResponse(['Forbidden'], 403);
    }

    public function response(array $errors)
    {
        return response()->json(new ResponseRepository(422, null, $errors, []), 422);
    }

    public function forbiddenResponse()
    {
        return new JsonResponse(['Forbidden'], 403);
    }
}
