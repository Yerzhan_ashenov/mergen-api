<?php

namespace App\Http\Requests;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if ($this->products) {
            foreach ($this->products as $i => $product) {
                $rules['products.' . $i . '.id'] = 'required|exists:products,id';
                $rules['products.' . $i . '.quantity'] = 'required|numeric';
            }
        } else {
            $rules['products.1.id'] = 'required|exists:products,id';
            $rules['products.1.quantity'] = 'required|numeric';
        }

        $rules['type_payment'] = 'integer|in:1,2';
        $rules['comment'] = 'string';
        $rules['delivery_type'] = 'integer|in:' . Order::DELIVERY_TYPE_COURIER;
        $rules['available_sum'] = 'numeric';
        $rules['delivery_address'] = 'string';
        $rules['delivery_date'] = 'date';
        $rules['promo_code'] = 'string';
        $rules['bonus'] = 'boolean';
        $rules['area_id'] = 'numeric|exists:delivery_areas,id';
        $rules['city_id'] = 'numeric|exists:cities,id';
        $rules['phone'] = 'required';
        $rules['name'] = 'required';

        return $rules;
    }
}
