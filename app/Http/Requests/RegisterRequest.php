<?php

namespace App\Http\Requests;


class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'     => 'required|unique:users',
            'platform'          => 'string|in:ios,android',
            'version'           => 'string',
            'model'             => 'string',
            'uuid'              => 'string|alpha_dash',
            'push_token'        => 'string'
        ];
    }
}
