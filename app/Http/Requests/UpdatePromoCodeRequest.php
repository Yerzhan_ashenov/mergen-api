<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePromoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            'cost' => 'required|numeric',
            'min_amount' => 'numeric',
            'expired_at' => 'date|required',
        ];

        if ($this->title == '') {
            $data['title'] = 'required|unique:promo_codes';
        }
        return $data;
    }
}
