<?php

namespace App\Http\Requests;


class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'first_name' => 'string',
            'last_name' => 'string',
            'middle_name' => 'string',
            'company' => 'string',
            'address' => 'string',
            'password' => 'string',
            'city_id' => 'integer',
            'email' =>  'string|email',
            'image_id' => 'integer'
        ];
    }
}
