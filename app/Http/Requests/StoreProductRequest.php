<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'image_id' => 'mimes:jpeg,png,jpg,gif,svg|max:20048',
            'unit_id' => 'required',
            'cost' => 'required',
            'category_id' => 'required',
            'status_id' => 'required',
            'unit_starting_point'   =>  'numeric'
        ];
    }
}
