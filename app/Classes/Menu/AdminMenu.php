<?php
namespace App\Classes\Menu;

class AdminMenu extends Menu
{
    public function __construct()
    {
        parent::__construct();

        $this->setLists([
            $this->ordersMenu(),
            $this->categoryMenu(),
            $this->productMenu(),
            $this->usersMenu(),
            $this->unitsMenu(),
            $this->newsMenu(),
            $this->promoMenu(),
            $this->areaMenu(),
        ]);
    }

    public function productMenu()
    {
        return new Menu('Продукция', '/admin/products','fa-list',[
            new Menu('Новая продукция', '/admin/products/create', 'fa-circle-o',[], 1),
            new Menu('Список продукции', '/admin/products', 'fa-circle-o',[], 1),
        ], 1);
    }

    public function categoryMenu()
    {
        return new Menu('Категории', '/admin/categories','fa-list',[
            new Menu('Новая категория', '/admin/categories/create', 'fa-circle-o',[], 1),
            new Menu('Список категории', '/admin/categories', 'fa-circle-o',[], 1),
        ], 1);
    }

    public function usersMenu()
    {
        return new Menu('Пользователи', '/admin/users','fa-circle-o', [
            new Menu('Новый пользователь', '/admin/users/create', 'fa-circle-o',[], 1),
            new Menu('Список', '/admin/users', 'fa-circle-o',[], 1),
        ],1);
    }

    public function unitsMenu()
    {
        return new Menu('Единицы измерения', '/admin/units','fa-circle-o', [
            new Menu('Добавить', '/admin/units/create', 'fa-circle-o',[], 1),
            new Menu('Список', '/admin/units', 'fa-circle-o',[], 1),
        ],1);
    }

    public function newsMenu()
    {
        return new Menu('Новости/Акции', '/admin/news','fa-circle-o', [
            new Menu('Добавить', '/admin/news/create', 'fa-circle-o',[], 1),
            new Menu('Список', '/admin/news', 'fa-circle-o',[], 1),
        ],1);
    }

    public function promoMenu()
    {
        return new Menu('Промо коды', '/admin/promo','fa-circle-o', [
            new Menu('Добавить', '/admin/promo/create', 'fa-circle-o',[], 1),
            new Menu('Список', '/admin/promo', 'fa-circle-o',[], 1),
        ],1);
    }

    public function areaMenu()
    {
        return new Menu('Районы доставки', '/admin/delivery-areas','fa-circle-o', [
            new Menu('Добавить', '/admin/delivery-areas/create', 'fa-circle-o',[], 1),
            new Menu('Список', '/admin/delivery-areas', 'fa-circle-o',[], 1),
        ],1);
    }

    public function ordersMenu()
    {
        return new Menu('Заказы', '/admin/orders','fa-circle-o', [
        ],1);
    }


}
