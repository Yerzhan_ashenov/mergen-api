<?php
namespace App\Classes\Menu;

class RoleMenu {

    private $role;

    public function __construct($role)
    {
        $this->role = $role;
    }


    public function render()
    {
        switch ($this->role->alias) {
            case 'admin':
                $menu = new AdminMenu();
                break;
            default:
                $menu = new BasicMenu();
                break;
        }

        return $menu->render();
    }
}