<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Product;
use App\Models\Unit;
use Dosarkz\LaravelUploader\BaseUploader;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Http\UploadedFile;

class DataProductsParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:upload';

    protected $client;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client(array(
            'curl' => array(CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false),
            'allow_redirects' => false,
            'cookies' => true,
            'verify' => false,
            'headers' => ['Authorization' => "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI5YjM4YTk4My0wMDIyLTQ4OWUtYmI5NC0zYmNmNzhlYjk0YWEiLCJpc3MiOiJlS2kxcVhqdUE4NFpIZlRSaU9hTlZoMGYyVDMyekFGQyIsImlhdCI6MTU4Nzg5NjE4NiwiZXhwIjo0NzQxNDk2MTg2LCJjb25zdW1lciI6eyJpZCI6ImU1YzRlYTA1LWY4ZTgtNDJiZC1iMDJhLWNmMzNlODAyZjA5NiIsIm5hbWUiOiJhcmJ1ei1rei53ZWIuZGVza3RvcCJ9LCJjaWQiOm51bGx9.LckxZHQgaWWz8D9ATBHMT_tlzgy-KkSA9K9PwuQXYPU"]
        ));

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->request([
            'method' => 'GET',
            'url' => 'https://arbuz.kz/api/v1/shop/catalog'
        ]);

        foreach ($data['data'] as $cat) {
            $model = $this->makeCategory($cat);
            $this->showCategory($cat['id'], $model->id);
        }

        $this->info('Well done ArbuZ! You are great!!!');
    }

    private function showCategory($id, $localId)
    {
        $data = $this->request([
            'method' => 'GET',
            'url' => 'https://arbuz.kz/api/v1/shop/catalog/' . $id
        ]);
        $catalogs = $data['data']['catalogs']['data'];

        foreach ($catalogs as $item) {
            $model = $this->makeCategory($item, $localId);
            $this->showCategory($item['id'], $model->id);
        }
        $products = $data['data']['products'];

        for($i = $products['page']['current']; $i <= $products['page']['last']; $i++)
        {
            $productData = $this->request([
                'method' => 'GET',
                'url' => 'https://arbuz.kz/api/v1/shop/catalog/' . $id . '?page='.$i
            ]);

            foreach ($productData['data']['products']['data'] as $product) {
                $this->makeProduct($product, $productData['data']['name']);
            }
        }

    }

    private function makeProduct($product)
    {
        $cat = Category::where('local_id', $product['catalogId'])->first();
        if(!$cat) {
            $this->error('skipped category initial product!');
            return false;
        }

        $model = Product::where('title', $product['name'])->first();

        $unit = Unit::firstOrCreate([
            'title' => $product['measure'],
        ]);

        if(! $model)
        {
            $description = $product['contents']['data'][0]['content'];

            $model = Product::create([
                'title' => $product['name'],
                'alias' => trans_url($product['name']),
                'user_id' => 1,
                'description' => $description,
                'cost' => $product['priceActual'],
                'category_id' => $cat->id,
                'status_id' => Product::STATUS_ACTIVE,
                'unit_id' => $unit->id
            ]);
        }else{
            $model->update([
                'unit_id' => $unit->id,
                'category_id' => $cat->id,
                'cost' => $product['priceActual'],
            ]);
        }

        if ($model->image == null && $product['image'] != null) {
            $icon = strstr($product['image'], '/w', true);
            $this->uploadImage($model, $icon);
        }

        $this->info('Product ' .$product['name'] . ' created');
        return $model;
    }


    private function makeCategory($item, $parentId = 0)
    {
        $icon = null;
        $model = Category::where('local_id', $item['id'])->first();

        if (!$model) {
            $model = Category::create([
                'title' => $item['name'],
                'parent_id' => $parentId,
                'status_id' => Category::STATUS_ACTIVE,
                'alias' => trans_url($item['name']) . '-'.$parentId,
                'local_id' => $item['id']
            ]);
        }

        if ($model->image == null && $item['iconPng'] != null) {
            $icon = strstr($item['iconPng'], '/w', true);
            $this->uploadImage($model, $icon);
        }

        return $model;
    }

    private function request($params)
    {
        $res = $this->client->request($params['method'], $params['url']);
        return json_decode($res->getBody(), true);
    }

    private function uploadImage($model, $image)
    {
        $info = pathinfo($image);
        $contents = file_get_contents($image);
        $file = '/tmp/' . $info['basename'];
        file_put_contents($file, $contents);
        $uploaded_file = new UploadedFile($file, $info['basename']);
        $file = BaseUploader::image($uploaded_file, 'uploads/icons', true, 1024, 768,
            null, 100);

        $model->image()->firstOrCreate([
            'name' => $file->getFileName(),
            'thumb' => $file->getThumb(),
            'path' => $file->getDestination()
        ]);
    }
}
