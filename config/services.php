<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => \App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'xmpp'=> [
        'host' => env('XMPP_HOST'),
        'port' => env('XMPP_PORT'),
        'user' => env('XMPP_USER'),
        'password'=> env('XMPP_PASSWORD'),
    ],
    'smscru' => [
        'login'  => env('SMSCRU_LOGIN'),
        'secret' => env('SMSCRU_SECRET'),
        'host'      => env('SMSCRU_HOST'),
        'sender' => 'al-fresco',
        'extra'  => [
            // any other API parameters
            // 'tinyurl' => 1
        ],
    ],

];
