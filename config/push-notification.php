<?php

return array(

    'bakeIOS'     => array(
        'environment' =>'development',
        'certificate' => app_path('Repositories/Notification/certificates/pushcert.pem'),
        'passPhrase'  => '',
        'service'     =>'apns'
    ),
    'bakeAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyC63RAGXGod7hoobEUxyuHXXHS_t5zQ3d4',
        'service'     =>'gcm'
    )

);