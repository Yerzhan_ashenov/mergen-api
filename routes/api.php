<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Api'],function(){

    Route::post('auth/confirm-sms', 'AuthController@confirmSms');
    Route::post('auth/register','AuthController@register');
    Route::post('auth/resend-sms', 'AuthController@resendSms');

    Route::resource('categories','CategoryController',['except' => [
        'create', 'store', 'edit', 'update', 'destroy']
    ]);

    Route::resource('news','NewsController',['only' => [
        'index', 'show']
    ]);

    Route::resource('categories.products','ProductController', ['parameters' => 'singular',
        'except' =>['create','store', 'edit', 'update', 'destroy']]);

    Route::get('cities/{city}/delivery-areas', 'AppController@getDeliveryAreas')->name('api.delivery-areas');
    Route::get('cities', 'AppController@getCities');

    Route::post('orders/guest', 'OrderController@storeGuest');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('promo-code', 'AppController@getPromoCode')->name('api.promo-code.check');
        Route::resource('users','UserController',['except' => [
            'create', 'store', 'edit', 'index']
        ]);

        Route::get('profile', 'UserController@profile');

        Route::resource('orders','OrderController',['except' => [
            'create',  'edit', 'update']
        ]);

        Route::post('orders/{order}/pay', 'OrderController@pay');
        Route::get('orders/{order}/pay-status', 'OrderController@payStatus');

        Route::group(['middleware' => 'role:admin'], function()
        {
            Route::get('admin/orders', 'OrderController@adminIndex');
            Route::post('admin/orders/{id}/moderate', 'OrderController@moderateOrder');
        });


    });

});
