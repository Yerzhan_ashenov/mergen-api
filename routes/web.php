<?php

Route::get('/',function(){
    return redirect(route('lora.auth.login'));
});

Route::get('payment-status', 'AppController@paymentStatus');
